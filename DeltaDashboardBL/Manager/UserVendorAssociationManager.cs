﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardDAL.Manager;
using System;
using System.Collections.Generic;

namespace DeltaDashboardBL.Manager
{
    public class UserVendorAssociationManager
    {
        public bool CreateUVAssociation(UserVendorAssociation uvAssociation)
        {
            try
            {
                new UserVendorAssociationDataManager().CreateUVAssociation(uvAssociation);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public UserVendorAssociation UpdateVendorAssociation(UserVendorAssociation uvAssociation)
        {
            return new UserVendorAssociationDataManager().UpdateUVAssociation(uvAssociation);
        }

        public List<Vendor> GetSelectedVendor(int userId)
        {
            return new UserVendorAssociationDataManager().GetSelectedVendor(userId);
        }

        public List<UserVendorAssociation> GetAssociationVendor(int userId)
        {
            return new UserVendorAssociationDataManager().GetAssociationVendor(userId);
        }

        public bool DeleteUVAssociatio(List<UserVendorAssociation> uvAssociation)
        {
            try
            {
                new UserVendorAssociationDataManager().DeleteUVAssociatio(uvAssociation);
                return true;
            }
            catch (Exception ex)    
            {
                return false;
            }
        }
    }
}
