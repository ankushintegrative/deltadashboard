﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardDAL.Manager;
using System;
using System.Collections.Generic;

namespace DeltaDashboardBL.Manager
{
    public class StatementApproveManager
    {
        public bool CreateStatementApprove(StatementApprove statementApprove)
        {
            try
            {
                new StatementApproveDataManager().CreateStatementApprove(statementApprove);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public StatementApprove GetDataByStatementId(int id)
        {
            return new StatementApproveDataManager().GetDataByStatementId(id);
        }

        public StatementApprove UpdateStatementApprove(StatementApprove statementApprove)
        {
            return new StatementApproveDataManager().UpdateStatementApprove(statementApprove);
        }
    }
}
