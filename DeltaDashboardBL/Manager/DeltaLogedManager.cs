﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardCommon.HelperViewModel;
using DeltaDashboardDAL.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardBL.Manager
{
   public class DeltaLogedManager
   {
        public int CreateDeltaLog(DeltaLoged deltaLoged)
        {
            try
            {
                int id = new DeltaLogedDataManager().CreateDeltaLog(deltaLoged);
                return id;
            }
            catch(Exception ex)
            {
                return 0;
            }            
        }

        public DeltaLoged UpdateStatus(DeltaLoged deltaLoged)
        {
            return new DeltaLogedDataManager().UpdateStatus(deltaLoged);
        }

        public List<DeltaLoged> GetNotApprovedDeltaLog(string LogType,string Status)
        {
            return new DeltaLogedDataManager().GetNotApprovedDeltaLog(LogType, Status);
        }

        public List<DeltaLoged> GetDataByChangeStatus(bool Status)
        {
            return new DeltaLogedDataManager().GetDataByChangeStatus(Status);
        }

        public DeltaLoged GetDataByChangeStatusDetails(int id)
        {
            return new DeltaLogedDataManager().GetDataByChangeStatusDetails(id);
        }

        public CommonHeaderLogModel GetCommonHeaderLogAPRepData(int APRepID)
        {
            return new DeltaLogedDataManager().GetCommonHeaderLogAPRepData(APRepID);
        }

        public List<DeltaLoged> GetDeltaLogByUser(string LogType, string Status, int CreatedBy, bool IsAdmin)
        {
            return new DeltaLogedDataManager().GetDeltaLogByUser(LogType, Status, CreatedBy,IsAdmin);
        }

        public List<VendorRepSummaryModel> GetVendorRepSummary()
        {
            return new DeltaLogedDataManager().GetVendorRepSummary();
        }

        public List<IndividualRepModel> GetIndividualRep(string APRepId)
        {
            return new DeltaLogedDataManager().GetIndividualRep(APRepId);
        }

        public List<Reasons> GetCreditHoldsReasons()
        {
            return new DeltaLogedDataManager().GetCreditHoldsReasons();
        }

        public List<Reasons> GetEscalationsReasons()
        {
            return new DeltaLogedDataManager().GetEscalationsReasons();
        }
        public List<VendorRepRef> GetVendorRepRef(string VendorId, string APRepId)
        {
            return new DeltaLogedDataManager().GetVendorRepRef(VendorId, APRepId);
        }

        public List<DeltaLoged> GetLoggedGridDataByAPRep(string LogType, string APrep, bool IsAdmin)
        {
            return new DeltaLogedDataManager().GetLoggedGridDataByAPRep(LogType, APrep, IsAdmin);
        }

        public List<DeltaLoged> GetHistoryDeltaLogByUser(string LogType, string APRep, bool IsAdmin)
        {
            return new DeltaLogedDataManager().GetHistoryDeltaLogByUser(LogType, APRep, IsAdmin);
        }

        public List<DeltaLoged> GetExportDataDeltaLogByUser(string LogType, string Status, int CreatedBy, bool IsAdmin)
        {
            return new DeltaLogedDataManager().GetExportDataDeltaLogByUser(LogType, Status, CreatedBy, IsAdmin);
        }
   }
}
