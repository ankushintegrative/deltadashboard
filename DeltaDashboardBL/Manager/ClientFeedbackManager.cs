﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardDAL.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardBL.Manager
{
    public class ClientFeedbackManager
    {
        public bool CreateFeedback(ClientFeedback clinetFeedback, string userName)
        {
            try
            {
                foreach (var feedback in clinetFeedback.feedbackType)
                {
                    clinetFeedback.CSRType = feedback.Id;
                    clinetFeedback.FeedbackOption = feedback.Option;
                    clinetFeedback.CreatedBy = userName;
                    clinetFeedback.CreatedDate = DateTime.Now;
                    new ClientFeedbackDataManager().CreateFeedback(clinetFeedback);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public ClientFeedback UpdateFeedback(ClientFeedback clinetFeedback)
        {
            return new ClientFeedbackDataManager().UpdateFeedback(clinetFeedback);
        }

        public List<ClientFeedback> GetClientFeedback(string month, string year)
        {
            return new ClientFeedbackDataManager().GetClientFeedback(month, year);
        }

        public List<ClientFeedback> GetAllClientFeedback()
        {
            return new ClientFeedbackDataManager().GetAllClientFeedback();
        }

        public List<User> GetPendingAPRep(string month, string year)
        {
            return new ClientFeedbackDataManager().GetPendingAPRep(month, year);
        }

        public List<User> GetPendingAPRepDataProcessing(string month, string year)
        {
            return new ClientFeedbackDataManager().GetPendingAPRepDataProcessing(month, year);
        }

        public List<User> GetCompleteAPRep(string month, string year)
        {
            return new ClientFeedbackDataManager().GetCompleteAPRep(month, year);
        }

        public List<ClientFeedback> GetDataByApRep(int aprepId, string month, string year)
        {
            return new ClientFeedbackDataManager().GetDataByApRep(aprepId, month, year);
        }
    }
}
