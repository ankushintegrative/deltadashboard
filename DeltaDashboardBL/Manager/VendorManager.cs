﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardDAL.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardBL.Manager
{
    public class VendorManager
    {
        public bool CreateVendor(Vendor vendor)
        {
            try
            {
                new VendorDataManager().CreateVendor(vendor);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Vendor UpdateVendor (Vendor vendor)
        {
            return new VendorDataManager().UpdateVendor(vendor);
        }

        public List<Vendor> GetVendorList()
        {
            return new VendorDataManager().GetVendorList();
        }

        public List<Vendor> GetVendorsByAPRep(int APRepUserId)
        {
            return new VendorDataManager().GetVendorsByAPRep(APRepUserId).ToList();
        }

        public Vendor GetVendorByNumber(string number)
        {
            return new VendorDataManager().GetVendorByNumber(number);
        }

        public Vendor GetVendorById(int id)
        {
            return new VendorDataManager().GetVendorById(id);
        }
    }
}
