﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeltaDashboardCommon.Domain;
using DeltaDashboardCommon.HelperViewModel;
using DeltaDashboardDAL.Manager;

namespace DeltaDashboardBL.Manager
{
    public class RewardManager
    {
        public int CreateReward(Reward reward)
        {
            try
            {
                int id = new RewardDataManager().CreateReward(reward);
                return id;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public Reward UpdateStatus(Reward reward)
        {
            return new RewardDataManager().UpdateStatus(reward);
        }
        public List<Reward> GetRewards()
        {
            return new RewardDataManager().GetRewards();
        }
    }
}
