﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardDAL.Manager;
using System;
using System.Collections.Generic;

namespace DeltaDashboardBL.Manager
{
    public class UserManager
    {
        public bool CreateUser(User user)
        {
            try
            {
                new UserDataManager().CreateUser(user);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public User UpdateUser(User user)
        {
            return new UserDataManager().UpdateUser(user);
        }

        public User ValidateLogin(string UserName, string Password)
        {
            return new UserDataManager().ValidateLogin(UserName, Password);
        }

        public User GetUserByUserName(string userName)
        {
            return new UserDataManager().GetUserByUserName(userName);
        }

        public List<User> GetUserList()
        {
            return new UserDataManager().GetUserList();
        }

        public List<Vendor> GetVendorList()
        {
            return new UserDataManager().GetVendorList();
        }

        public List<UserRole> GetUserRolesList()
        {
            return new UserDataManager().GetUserRolesList();
        }

        public List<POD> GetPODList()
        {
            return new UserDataManager().GetPODList();
        }
    }
}
