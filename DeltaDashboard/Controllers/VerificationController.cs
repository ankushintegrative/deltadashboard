﻿using DeltaDashboard.Models;
using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class VerificationController : Controller
    {
        // GET: Verification
        public ActionResult Verification()
        {
            return View();
        }

        public JsonResult GetGridData(string LogType, string Status)
        {
            return Json(new DeltaLogedManager().GetNotApprovedDeltaLog(LogType, Status).OrderByDescending(o => o.CreatedDate), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataByChangeStatus(string Status)
        {
            bool isStatus = false;
            if (!string.IsNullOrEmpty(Status))
            {
                if (Convert.ToInt32(Status) == 1) //Pending
                    isStatus = false;
                else if (Convert.ToInt32(Status) == 2) //Approve
                    isStatus = true;
                else if (Convert.ToInt32(Status) == 0) //All
                    return Json(new DeltaLogedManager().GetNotApprovedDeltaLog("", ""), JsonRequestBehavior.AllowGet);
            }

            return Json(new DeltaLogedManager().GetDataByChangeStatus(isStatus), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateApproved(List<VerificationViewModel> StatusCheckList)
        {
            foreach (VerificationViewModel verification in StatusCheckList)
            {
                DeltaLoged verificationObj = new DeltaLogedManager().GetDataByChangeStatusDetails(verification.Index);
                if(verificationObj != null) {
                    var statementApproveManager = new StatementApproveManager().GetDataByStatementId(verificationObj.Id);

                    verificationObj.ApprovedBy = Convert.ToInt32(Session["UserId"]);
                    verificationObj.ApprovedDate = DateTime.Now;

                    if (statementApproveManager != null)
                    {
                        if (statementApproveManager.RequestedDate != null || statementApproveManager.ReceivedDate != null || statementApproveManager.MatchCompleteDate != null || statementApproveManager.SettlementDate != null)
                            verificationObj.IsApproved = verification.Checked;

                        statementApproveManager.StatementId = verificationObj.Id;
                        if (statementApproveManager.RequestedDate != null && statementApproveManager.RequestedBy == null)
                        {
                            statementApproveManager.isRequested = true;
                            statementApproveManager.RequestedBy = Convert.ToInt32(Session["UserId"]);
                            statementApproveManager.RequestedApprovedDate = DateTime.Now;
                        }
                        if (statementApproveManager.ReceivedDate != null && statementApproveManager.ReceivedBy == null)
                        {
                            statementApproveManager.isReceived = true;
                            statementApproveManager.ReceivedBy = Convert.ToInt32(Session["UserId"]);
                            statementApproveManager.ReceivedApprovedDate = DateTime.Now;
                        }
                        if (statementApproveManager.MatchCompleteDate != null && statementApproveManager.MatchCompleteBy == null)
                        {
                            statementApproveManager.isMatchComplete = true;
                            statementApproveManager.MatchCompleteBy = Convert.ToInt32(Session["UserId"]);
                            statementApproveManager.MatchCompleteApprovedDate = DateTime.Now;
                        }
                        if (statementApproveManager.SettlementDate != null && statementApproveManager.SettlementBy == null)
                        {
                            statementApproveManager.isSettlementDate = true;
                            statementApproveManager.SettlementBy = Convert.ToInt32(Session["UserId"]);
                            statementApproveManager.SettlementApprovedDate = DateTime.Now;
                        }
                        new StatementApproveManager().UpdateStatementApprove(statementApproveManager);
                    }
                    else {
                        verificationObj.IsApproved = verification.Checked;
                    }

                    new DeltaLogedManager().UpdateStatus(verificationObj);
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}