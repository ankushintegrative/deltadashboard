﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class StatementReceiptsController : Controller
    {
        // GET: StatementReceipts
        public ActionResult StatementReceipt()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StatementReceipt(DeltaLoged deltaLoged)
        {
            deltaLoged.CreatedBy = (int)Session["UserId"];
            deltaLoged.CreatedDate = DateTime.Now;
            deltaLoged.IsApproved = false;
            deltaLoged.LogedDataFor = "Statement Receipt";

            if (deltaLoged.Id == 0)
            {
                int id = new DeltaLogedManager().CreateDeltaLog(deltaLoged);
                if (id >= 0)
                {
                    var statementApproveManager = new StatementApproveManager().GetDataByStatementId(id);
                    StatementApprove statementApprove = new StatementApprove();
                    if (statementApproveManager == null)
                    {
                        statementApprove.StatementId = id;
                        if (deltaLoged.StatementRequestedDate != null)
                        {
                            statementApprove.RequestedDate = deltaLoged.StatementRequestedDate;
                        }
                        if (deltaLoged.StatementReceivedDate != null)
                        {
                            statementApprove.ReceivedDate = deltaLoged.StatementReceivedDate;
                        }
                        if (deltaLoged.StatementMatchCompleteDate != null)
                        {
                            statementApprove.MatchCompleteDate = deltaLoged.StatementMatchCompleteDate;
                        }
                        if (deltaLoged.SettlementDate != null)
                        {
                            statementApproveManager.SettlementDate = deltaLoged.SettlementDate;
                        }
                        new StatementApproveManager().CreateStatementApprove(statementApprove);
                    }
                }
            }
            else
            {
                new DeltaLogedManager().UpdateStatus(deltaLoged);
                var statementApproveManager = new StatementApproveManager().GetDataByStatementId(deltaLoged.Id);
                if (statementApproveManager != null)
                {
                    statementApproveManager.StatementId = deltaLoged.Id;
                    if (deltaLoged.StatementRequestedDate != null)
                    {
                        statementApproveManager.RequestedDate = deltaLoged.StatementRequestedDate;
                    }
                    if (deltaLoged.StatementReceivedDate != null)
                    {
                        statementApproveManager.ReceivedDate = deltaLoged.StatementReceivedDate;
                    }
                    if (deltaLoged.StatementMatchCompleteDate != null)
                    {
                        statementApproveManager.MatchCompleteDate = deltaLoged.StatementMatchCompleteDate;
                    }
                    if (deltaLoged.SettlementDate != null)
                    {
                        statementApproveManager.SettlementDate = deltaLoged.SettlementDate;
                    }
                    new StatementApproveManager().UpdateStatementApprove(statementApproveManager);
                }
            }

            return RedirectToAction("StatementReceipt", "StatementReceipts");
        }

        //public JsonResult GetStatementReceiptData()
        //{
        //    var Statement = new DeltaLogedManager().GetDeltaLogByUser("Statement Receipt", "0", (int)Session["UserId"]);
        //    return Json(Statement, JsonRequestBehavior.AllowGet);
        //}
    }
}