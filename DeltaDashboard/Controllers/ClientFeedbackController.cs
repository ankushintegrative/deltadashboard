﻿using ClosedXML.Excel;
using ClosedXML.Excel.Drawings;
using DeltaDashboard.Helper;
using DeltaDashboard.Models;
using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class ClientFeedbackController : Controller
    {
        // GET: ClientFeedback
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CreateFeedback(ClientFeedback clientFeedback)
        {
            ClientFeedbackManager clientFeedbackManager = new ClientFeedbackManager();
            clientFeedbackManager.CreateFeedback(clientFeedback, Session["FullName"].ToString());            
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAPRepUsersList()
        {
            if(Session["Role"].ToString() == "2" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "5")
                return Json(new UserManager().GetUserList().Where(o => o.Role == "1").ToList(), JsonRequestBehavior.AllowGet);
            else
                return Json(new UserManager().GetUserList().Where(o => o.Role == "1" && o.Id == Convert.ToInt32(Session["UserId"])).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDataByApRep(int aprepId, string month, string year)
        {
            ClientFeedbackManager clientFeedbackManager = new ClientFeedbackManager();
            List<ClientFeedback> clientFeedbackRecords =  clientFeedbackManager.GetDataByApRep(aprepId, month, year);
            return Json(clientFeedbackRecords, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPendingAPRep(string month, string year)
        {
            ClientFeedbackManager clientFeedbackManager = new ClientFeedbackManager();
            if (Session["Role"].ToString() == "2" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "5")
            {
                List<User> clientFeedbackPending = clientFeedbackManager.GetPendingAPRep(month, year);
                return Json(clientFeedbackPending.Where(o => o.Role == "1").OrderBy(o => o.APRepName), JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<User> clientFeedbackPending = clientFeedbackManager.GetPendingAPRep(month, year);
                //return Json(clientFeedbackPending.Where(o => o.Role == "1" && o.Id == Convert.ToInt32(Session["UserId"])).ToList(), JsonRequestBehavior.AllowGet);
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetCompleteAPRep(string month, string year)
        {
            ClientFeedbackManager clientFeedbackManager = new ClientFeedbackManager();
            if (Session["Role"].ToString() == "2" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "5")
            {
                List<User> clientFeedbackComplete = clientFeedbackManager.GetCompleteAPRep(month, year);
                return Json(clientFeedbackComplete.Where(o => o.Role == "1").OrderBy(o => o.APRepName), JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<User> clientFeedbackComplete = clientFeedbackManager.GetCompleteAPRep(month, year);
                //return Json(clientFeedbackComplete.Where(o => o.Role == "1" && o.Id == Convert.ToInt32(Session["UserId"])).ToList(), JsonRequestBehavior.AllowGet);
                return Json(0, JsonRequestBehavior.AllowGet);
            }            
        }

        public JsonResult GetClientFeedbackGridData(string month, string year)
        {
            ClientFeedbackManager clientFeedbackManager = new ClientFeedbackManager();
            var clientFeedback = clientFeedbackManager.GetClientFeedback(month, year).GroupBy(o => new { o.ApRepId, o.Month });
            List<ClientFeedbackGridModel> clientFeedbackModelList = new List<ClientFeedbackGridModel>();
            foreach (var clientfeedbk in clientFeedback) {
                ClientFeedbackGridModel clientFeedbackModel = new ClientFeedbackGridModel();
                clientFeedbackModel.ApRepName = clientfeedbk.Select(o => o.ApRepName).First();
                clientFeedbackModel.ApRepId = Convert.ToInt32(clientfeedbk.Select(o => o.ApRepId).First());

                var Rating = Convert.ToDecimal(clientfeedbk.Sum(o => o.FeedbackOption)).ToString("0.00");
                clientFeedbackModel.Rating = Math.Round(Convert.ToDecimal(Rating) / 7, 1);

                clientFeedbackModel.RatingBy = clientfeedbk.Select(o => o.CreatedBy).First();

                DateTime createdDate = clientfeedbk.Select(o => o.CreatedDate).First();
                clientFeedbackModel.RatingDate = createdDate.ToShortDateString();
                clientFeedbackModel.Month = clientfeedbk.Select(o => o.Month).First();
                clientFeedbackModel.Year = clientfeedbk.Select(o => o.Year).First();

                clientFeedbackModel.RoleId = clientfeedbk.Select(o => o.RoleId).First();
                clientFeedbackModel.UserId = clientfeedbk.Select(o => o.UserId).First();

                clientFeedbackModelList.Add(clientFeedbackModel);
            }
            if (Session["Role"].ToString() == "2" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "5")
            {
                TempData["ExportToExcelReport"] = GetAllClientFeedback().Where(o => o.RoleId == "1").ToList();
                return Json(clientFeedbackModelList.Where(o => o.RoleId == "1").OrderBy(o => o.ApRepName), JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["ExportToExcelReport"] = GetAllClientFeedback().Where(o => o.RoleId == "1" && o.UserId == Convert.ToInt32(Session["UserId"])).ToList();
                return Json(clientFeedbackModelList.Where(o => o.RoleId == "1" && o.UserId == Convert.ToInt32(Session["UserId"])).OrderBy(o => o.ApRepName).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public List<ClientFeedbackGridModel> GetAllClientFeedback()
        {
            ClientFeedbackManager clientFeedbackManager = new ClientFeedbackManager();
            var clientFeedback = clientFeedbackManager.GetAllClientFeedback().GroupBy(o => new { o.ApRepId, o.Month });
            List<ClientFeedbackGridModel> clientFeedbackModelList = new List<ClientFeedbackGridModel>();
            foreach (var clientfeedbk in clientFeedback)
            {
                ClientFeedbackGridModel clientFeedbackModel = new ClientFeedbackGridModel();
                clientFeedbackModel.ApRepName = clientfeedbk.Select(o => o.ApRepName).First();
                clientFeedbackModel.ApRepId = Convert.ToInt32(clientfeedbk.Select(o => o.ApRepId).First());

                var Rating = Convert.ToDecimal(clientfeedbk.Sum(o => o.FeedbackOption)).ToString("0.00");
                clientFeedbackModel.Rating = Math.Round(Convert.ToDecimal(Rating) / 7, 1);

                clientFeedbackModel.RatingBy = clientfeedbk.Select(o => o.CreatedBy).First();
                DateTime createdDate = clientfeedbk.Select(o => o.CreatedDate).First();
                clientFeedbackModel.RatingDate = createdDate.ToShortDateString();
                clientFeedbackModel.Month = clientfeedbk.Select(o => o.Month).First();
                clientFeedbackModel.Year = clientfeedbk.Select(o => o.Year).First();
                clientFeedbackModel.RoleId = clientfeedbk.Select(o => o.RoleId).First();
                clientFeedbackModel.UserId = clientfeedbk.Select(o => o.UserId).First();
                clientFeedbackModelList.Add(clientFeedbackModel);
            }
            return clientFeedbackModelList.OrderBy(o => o.ApRepName).ToList();
        }

        public FileResult ExportToExcel()
        {
            if (TempData["ExportToExcelReport"] != null)
            {
                List<ClientFeedbackGridModel> ExportToExcelReport = (List<ClientFeedbackGridModel>)TempData["ExportToExcelReport"];
                TempData["ExportToExcelReport"] = ExportToExcelReport;

                DataTable tblExportToExcel = GetExportData(ExportHelper.GetExportDataTable(new DataTable("Client Feedback Data")), ExportToExcelReport);
                XLExporter vksxl = new XLExporter();

                using (XLWorkbook wb = vksxl.ClientFeedbackExport2Excel("Client Feedback Data Download", XLColor.White, XLColor.Black, 15, true, "",
                "", "", XLColor.White, XLColor.Black, 10, tblExportToExcel, XLColor.Brown, XLColor.White, "Client Feedback Data", "ClientFeedback_Data_Download.xlsx", null))
                {
                    string path = Server.MapPath("~/Content/Images/Delta logo.png");
                    byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                    string imageBase64Data = Convert.ToBase64String(imageByteData);
                    string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);

                    byte[] myByte = new byte[10];

                    MemoryStream theMemStream = new MemoryStream();

                    theMemStream.Write(imageByteData, 0, imageByteData.Length);


                    #region Png

                    var ws = wb.Worksheet("Client Feedback Data");

                    ws.AddPicture(theMemStream, XLPictureFormat.Png, "PngImage")
                            .MoveTo(ws.Cell("A2").Address).WithSize(200, 65);
                    // ws.Cell("A1").Value = Title;

                    #endregion Png

                    // wb.Worksheets.Add(tblExportToExcel).Tables.FirstOrDefault().ShowAutoFilter = false;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ClientFeedback_Data_Download.xlsx");
                    }

                }
            }
            return null;

        }

        private DataTable GetExportData(DataTable dt, List<ClientFeedbackGridModel> ExportToExcelReport)
        {
            foreach (var clientFB in ExportToExcelReport)
            {
                dt.Rows.Add(clientFB.ApRepName, clientFB.Rating, clientFB.Month, clientFB.Year, clientFB.RatingBy, clientFB.RatingDate);
            }

            return dt;
        }

        public static class ExportHelper
        {
            public static DataTable GetExportDataTable(DataTable dt)
            {
                dt.Columns.AddRange(new DataColumn[1] { new DataColumn("ApRepName") });
                dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Rating") });
                dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Month") });
                dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Year") });
                dt.Columns.AddRange(new DataColumn[1] { new DataColumn("RatingBy") });
                dt.Columns.AddRange(new DataColumn[1] { new DataColumn("RatingDate") });
                return dt;
            }

        }
    }
}