﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class VendorController : Controller
    {
        // GET: Vendor
        public ActionResult ManageVendor()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CreateVendor(Vendor vendor)
        {
            VendorManager vendorManager = new VendorManager();
            Vendor vnumber = vendorManager.GetVendorByNumber(vendor.VendorNumber);
            if (vnumber == null)
            {
                vendorManager.CreateVendor(vendor);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateVendor(Vendor vendor)
        {
            try
            {
                VendorManager vendorManager = new VendorManager();
                Vendor vnumber = vendorManager.GetVendorById(vendor.Id);
                if (vnumber != null)
                {
                    if (vnumber.VendorNumber == vendor.VendorNumber)
                    {
                        vendorManager.UpdateVendor(vendor);
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }

                    Vendor vennumber = vendorManager.GetVendorByNumber(vendor.VendorNumber);
                    if (vennumber == null)
                    {
                        vendorManager.UpdateVendor(vendor);
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetVendorList()
        {
            List<Vendor> VendorList = new VendorManager().GetVendorList().OrderBy(o => o.VendorName).ToList();
            List<Vendor> VendorDetails = new List<Vendor>();
            foreach (Vendor vendor in VendorList)
            {
                Vendor vendorObj = new Vendor();
                vendorObj.Id = vendor.Id;
                vendorObj.VendorName = vendor.VendorName;
                vendorObj.VendorNumber = vendor.VendorNumber;
                vendorObj.isStatement = vendor.isStatement;
                vendorObj.StatementReq = vendor.isStatement == true ? "YES" : "NO";
                VendorDetails.Add(vendorObj);
            }
            return Json(VendorDetails, JsonRequestBehavior.AllowGet);
        }
    }
}