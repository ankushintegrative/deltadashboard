﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DeltaDashboard.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string UserName, string Password)
        {
            User loginModel = new User();
            if (String.IsNullOrWhiteSpace(UserName) || String.IsNullOrWhiteSpace(Password))
            {
                loginModel.IsValidUser = false;
                loginModel.ErrorMsg = "Please provide User Id and Password.";
            }
            else
            {
                loginModel = new UserManager().ValidateLogin(UserName, Password);

                if (loginModel != null)
                {
                    Session["UserId"] = loginModel.Id;
                    Session["UserName"] = loginModel.UserName;
                    Session["FullName"] = loginModel.FullName;
                    Session["Role"] = loginModel.Role;
                }
                return Json(loginModel, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            Session["UserName"] = null;
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult SessionExpire()
        {
            return View();
        }
    }
}