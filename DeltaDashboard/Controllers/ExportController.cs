﻿using ClosedXML.Excel;
using ClosedXML.Excel.Drawings;
using DeltaDashboard.Helper;
using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class ExportController : Controller
    {
        // GET: Export
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetGridData(string logtype, string status, int userId, bool IsAdmin = true, string logtypeName = "", string statusName = "", string userName = "")
        {

            List<DeltaLoged> ExportData = new DeltaLogedManager().GetExportDataDeltaLogByUser(logtype, status, userId, IsAdmin).OrderBy(o => o.APRepName).ToList();

            ExcelParaModel objExcelParaModel = new ExcelParaModel();
            objExcelParaModel.Logtype = logtypeName == "Statement Receipt" ? "Statement" : logtypeName;
            objExcelParaModel.UserName = userName;
            objExcelParaModel.Status = statusName;
            TempData["ExcelParaModel"] = objExcelParaModel;
            TempData["ExportToExcelReport"] = ExportData;

            return Json(ExportData, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportToExcel()
        {
            if (TempData["ExportToExcelReport"] != null)
            {
                List<DeltaLoged> ExportToExcelReport = (List<DeltaLoged>)TempData["ExportToExcelReport"];
                TempData["ExportToExcelReport"] = ExportToExcelReport;
                var objExcelParaModel = TempData["ExcelParaModel"] != null ? (ExcelParaModel)TempData["ExcelParaModel"] : null;
                TempData["ExcelParaModel"] = objExcelParaModel;

                DataTable tblExportToExcel = GetExportData(ExportHelper.GetExportDataTable(new DataTable("Delta Dashboard Data")), ExportToExcelReport);
                XLExporter vksxl = new XLExporter();

                using (XLWorkbook wb = vksxl.Export2Excel("Delta Dashboard Data Download", XLColor.White, XLColor.Black, 15, true, objExcelParaModel.Status != null ? objExcelParaModel.Status : "All",
                objExcelParaModel.Logtype != null ? objExcelParaModel.Logtype : "", objExcelParaModel.UserName != null ? objExcelParaModel.UserName : "",
                XLColor.White, XLColor.Black, 10, tblExportToExcel, XLColor.Brown, XLColor.White, "Delta Dashboard Data", "DeltaDashboard_Data_Download.xlsx", null))
                {
                    string path = Server.MapPath("~/Content/Images/Delta logo.png");
                    byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                    string imageBase64Data = Convert.ToBase64String(imageByteData);
                    string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);

                    byte[] myByte = new byte[10];

                    MemoryStream theMemStream = new MemoryStream();

                    theMemStream.Write(imageByteData, 0, imageByteData.Length);


                    #region Png

                    var ws = wb.Worksheet("Delta Dashboard Data");

                    ws.AddPicture(theMemStream, XLPictureFormat.Png, "PngImage")
                            .MoveTo(ws.Cell("A2").Address).WithSize(200, 65);
                    // ws.Cell("A1").Value = Title;

                    #endregion Png

                    // wb.Worksheets.Add(tblExportToExcel).Tables.FirstOrDefault().ShowAutoFilter = false;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Delta_Dashboard_Data_Download.xlsx");
                    }

                }
            }
            return null;

        }

        private DataTable GetExportData(DataTable dt, List<DeltaLoged> ExportToExcelReport)
        {
            foreach (var DeltaLoged in ExportToExcelReport)
            {
                dt.Rows.Add(DeltaLoged.StatusString, DeltaLoged.LogedDataFor == "Statement Receipt" ? "Statement" : DeltaLoged.LogedDataFor, DeltaLoged.DisplayCreatedDate, DeltaLoged.VendorName, DeltaLoged.VendorNumber, DeltaLoged.APRepName, DeltaLoged.PODName, DeltaLoged.SupervisorName, DeltaLoged.DisplayCreditHoldOn, DeltaLoged.DisplayCreditHoldOff, DeltaLoged.ReasonforCredithold, DeltaLoged.ReasonforEscalation, DeltaLoged.ActionPlanToPreventEscalation, DeltaLoged.DisplayStatementRequestedDate, DeltaLoged.DisplayStatementReceivedDate, DeltaLoged.DisplayStatementMatchCompleteDate);
            }

            return dt;
        }


        //public JsonResult ExportToExcel(string logtype, string status, int userId, bool IsAdmin = true)
        //{
        //    //  ExcelParaModel objExcelParaModel = new ExcelParaModel();

        //    var wb = new XLWorkbook();
        //    var ExportData= new DeltaLogedManager().GetExportDataDeltaLogByUser(logtype, status, userId, IsAdmin).OrderBy(o => o.APRepName).ToList();
        //    var dataTable = ToDataTable(ExportData);

        //    // Add a DataTable as a worksheet
        //    wb.Worksheets.Add(dataTable);       
        //   wb.SaveAs(Server.MapPath("/ExportedData/DeltaDashbord.xlsx"));
        //    return Json("~/ExportedData/DeltaDashbord.xlsx", JsonRequestBehavior.AllowGet);
        //}

        //public DataTable ToDataTable<T>(List<T> items)
        //{
        //    DataTable dataTable = new DataTable(typeof(T).Name);

        //    //Get all the properties
        //    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        //    foreach (PropertyInfo prop in Props)
        //    {
        //        //Defining type of data column gives proper data table 
        //        var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
        //        //Setting column names as Property names
        //        dataTable.Columns.Add(prop.Name, type);
        //    }
        //    foreach (T item in items)
        //    {
        //        var values = new object[Props.Length];
        //        for (int i = 0; i < Props.Length; i++)
        //        {
        //            //inserting property values to datatable rows
        //            values[i] = Props[i].GetValue(item, null);
        //        }
        //        dataTable.Rows.Add(values);
        //    }
        //    //put a breakpoint here and check datatable
        //    return dataTable;
        //}

    }

    public class ExcelParaModel
    {
        public String Status { get; set; }

        public String Logtype { get; set; }

        public String UserName { get; set; }

    }
    public static class ExportHelper
    {
        public static DataTable GetExportDataTable(DataTable dt)
        {
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Approved") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Log Type") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Date Log") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Vendor Name") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Vendor Number") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("APRep") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("POD") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Supervisor Name") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Credit On Hold") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Credit Off Hold") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Reason for Credit hold") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Reason for Escalation") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Action Plan To Prevent Escalation") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Statement Requested Date") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Statement Received Date") });
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Statement Match Complete Date") });
            return dt;
        }

    }

}