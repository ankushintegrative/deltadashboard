﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class EscalationController : Controller
    {
        public ActionResult Escalation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Escalation(DeltaLoged deltaLoged)
        {
            deltaLoged.CreatedBy = (int)Session["UserId"];
            deltaLoged.CreatedDate = DateTime.Now;
            deltaLoged.IsApproved = false;
            deltaLoged.LogedDataFor = "Escalation";
            if (deltaLoged.ReasonforEscalation == "Other")
            {
                deltaLoged.OtherReason = "Other-" + deltaLoged.OtherReason;
                deltaLoged.ReasonforEscalation = deltaLoged.OtherReason;
            }
            if (deltaLoged.Id == 0)
                new DeltaLogedManager().CreateDeltaLog(deltaLoged);
            else
                new DeltaLogedManager().UpdateStatus(deltaLoged);

            return RedirectToAction("Escalation", "Escalation");
        }

        // GET: Escalation
        public ActionResult EscalationStatus()
        {
            return View();
        }

        // GET: Escalation
        public ActionResult EscalationHistory()
        {
            return View();
        }

        public JsonResult GetEscalationData()
        {
            bool IsAdmin = false;

            if (Session["Role"].ToString() == "5" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "2")
                IsAdmin = true;
            var EscalationData = new DeltaLogedManager().GetDeltaLogByUser("Escalation", "0", (int)Session["UserId"], IsAdmin).OrderByDescending(o => o.CreatedDate);
            return Json(EscalationData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEscalationHistoryData()
        {
            bool IsAdmin = false;

            if (Session["Role"].ToString() == "5" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "2")
                IsAdmin = true;
            var EscalationData = new DeltaLogedManager().GetHistoryDeltaLogByUser("Escalation",  Session["UserId"].ToString(), IsAdmin).OrderByDescending(o => o.CreatedDate);
            return Json(EscalationData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetEscalationsResonsList()
        {

            return Json(new DeltaLogedManager().GetEscalationsReasons(), JsonRequestBehavior.AllowGet);
        }

    }
}