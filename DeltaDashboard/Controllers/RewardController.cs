﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class RewardController : Controller
    {
        // GET: Reward
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAPRepUsersList()
        {
            return Json(new UserManager().GetUserList().Where(o => o.Role != "5").ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPODList()
        {
            UserManager userManager = new UserManager();
            var podList = userManager.GetPODList().ToList().Where(o => o.Name != "Maroon");
            List<POD> lstpod = new List<POD>();
            foreach(var varpod in podList)
            {
                POD pod = new POD();
                pod.Id = varpod.Id;
                pod.Name = varpod.Name;
                pod.FullName = varpod.FullName;
                lstpod.Add(pod);
            }
            return Json(lstpod, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRewardList()
        {
            RewardManager rewardManager = new RewardManager();
            var rewradList = rewardManager.GetRewards().ToList().OrderByDescending(x => x.Id);
            List<Reward> rewards = new List<Reward>();
            foreach(var reward in rewradList)
            {
                Reward rwd = new Reward();
                rwd.Category = reward.Category;
                rwd.Comments = reward.Comments;
                rwd.RewardTo = reward.RewardTo;
                rwd.RewardDate = reward.RewardDate;
                rwd.RewardDateDisplay = reward.RewardDateDisplay;
                rewards.Add(rwd);
;
            }
            return Json(rewards, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Reward(Reward reward)
        {
            reward.CreatedBy = (int)Session["UserId"];
            reward.CreatedDate = DateTime.Now;
            if (reward.Id == 0)
                new RewardManager().CreateReward(reward);
            else
                new RewardManager().UpdateStatus(reward);

            return Json(reward);
        }
    }
}