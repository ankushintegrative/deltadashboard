﻿using DeltaDashboard.Models;
using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class RepresentativeController : Controller
    {
        // GET: IndividualRep
        public ActionResult VendorRepSummary()
        {
            return View();
        }

        public ActionResult RepresentativeVendorRef()
        {
            return View();
        }

        public JsonResult GetVendorRepRef()
        {
            return Json(new DeltaLogedManager().GetVendorRepRef("", "").OrderBy(o => o.APRepName), JsonRequestBehavior.AllowGet);
        }
        public ActionResult IndividualRepDetail()
        {
            return View();
        }

        public JsonResult GetGridData(string APRepId)
        {
            var Data = new DeltaLogedManager().GetIndividualRep(APRepId);           
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVendorRepSummary()
        {
            var vendorRepSummaryModel = new DeltaLogedManager().GetVendorRepSummary().OrderBy(o => o.APRepName);           

            return Json(vendorRepSummaryModel, JsonRequestBehavior.AllowGet);
        }

        private string ShiftName(string ShiftId)
        {
            string ShiftName = string.Empty;
            switch (ShiftId)
            {
                case "1":
                    ShiftName = "Regular";
                    break;
                case "2":
                    ShiftName = "Night";
                    break;
                default:
                    break;
            }
            return ShiftName;
        }
    }
}