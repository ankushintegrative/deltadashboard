﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class DeltaDashboardController : Controller
    {
        // GET: DeltaDashboard
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DeltaIndex()
        {
            User user = new UserManager().GetUserByUserName(Session["UserName"].ToString());
            if (user == null)
                return null;

            return Json(user, JsonRequestBehavior.AllowGet);
        }
    }
}