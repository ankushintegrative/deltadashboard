﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;
using TimeIsMoneyCommon.HelperClass;
using static TimeIsMoneyCommon.HelperClass.Enums;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class UserController : Controller
    {
        // GET: ManageUser
        public ActionResult ManageUser()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CreateUser(User user)
        {
            UserManager userManager = new UserManager();
            User userName = userManager.GetUserByUserName(user.UserName);
            if (userName == null)
            {
                userManager.CreateUser(user);

                UserVendorAssociationManager uvaManager = new UserVendorAssociationManager();
                User userNameInfo = userManager.GetUserByUserName(user.UserName);
                if (userNameInfo != null)
                {
                    UserVendorAssociation uvAssociation = new UserVendorAssociation(); 
                    foreach(var selectedVendor in user.SelectedVendor)
                    {
                        uvAssociation.VendorId = selectedVendor.Id;
                        uvAssociation.UserId = userNameInfo.Id;
                        uvaManager.CreateUVAssociation(uvAssociation);
                    }
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateUser(User user)
        {
            try
            {
                user = new UserManager().UpdateUser(user);
                UserVendorAssociation uvAssociation = new UserVendorAssociation();
                UserVendorAssociationManager uvaManager = new UserVendorAssociationManager();

                List<UserVendorAssociation> associationVendor = new UserVendorAssociationManager().GetAssociationVendor(user.Id);
                if (associationVendor != null)
                    uvaManager.DeleteUVAssociatio(associationVendor);

                foreach (var selectedVendor in user.SelectedVendor)
                {
                    uvAssociation.VendorId = selectedVendor.Id;
                    uvAssociation.UserId = user.Id;
                    uvaManager.CreateUVAssociation(uvAssociation);
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSelectedVendor(string userId)
        {
            List<Vendor> selectedVendor = new UserVendorAssociationManager().GetSelectedVendor(Convert.ToInt32(userId));
            return Json(selectedVendor, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserList()
        {
            List<User> User = new UserManager().GetUserList().OrderBy(o => o.FullName).ToList();
            List<User> UserDt = User.Where(o => o.Role != Convert.ToString((int)Role.Admin)).ToList();
            List<User> UserDetails = new List<User>();
            foreach (User user in UserDt) {
                User userObj = new User();
                userObj.Id = user.Id;
                userObj.FullName = user.FullName;
                userObj.UserName = user.UserName;
                userObj.EmailId = user.EmailId;
                userObj.Password = user.Password;
                userObj.Role = user.Role;
                userObj.RoleName = RoleName(user.Role);
                userObj.ShiftId = user.ShiftId;
                userObj.PodId = user.PodId;
                userObj.SupervisorId = user.SupervisorId;
                userObj.isDeltaTeam = user.isDeltaTeam;
                UserDetails.Add(userObj);
            }
            return Json(UserDetails, JsonRequestBehavior.AllowGet);
        }

        private string RoleName(string role)
        {
            string RoleName = string.Empty;
            switch (role)
            {
                case "1":
                    RoleName = "AP Representative";
                    break;
                case "2":
                    RoleName = "POD Lead";
                    break;
                case "3":
                    RoleName = "Team Lead";
                    break;
                case "4":
                    RoleName = "Supervisor";
                    break;
                case "6":
                    RoleName = "Data Processing";
                    break;
                default:
                    break;
            }
            return RoleName;
        }

        public JsonResult GetNonAlphaVendorList()
        {
            List<Vendor> UserDetails = new UserManager().GetVendorList();
            List<Vendor> alphaVendor = UserDetails.Where(o => o.VendorName.Contains("Alpha")).ToList();
            List<Vendor> nonAlphaVendor = UserDetails.Except(alphaVendor).ToList();
            return Json(nonAlphaVendor, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAlphaVendorList()
        {
            List<Vendor> UserDetails = new UserManager().GetVendorList();
            List<Vendor> alphaVendor = UserDetails.Where(o => o.VendorName.Contains("Alpha")).ToList();
            return Json(alphaVendor, JsonRequestBehavior.AllowGet);
        }
    }
}