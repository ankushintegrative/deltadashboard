﻿using DeltaDashboardBL.Manager;
using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class CreditHoldController : Controller
    {

        public ActionResult CreditHold()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreditHold(DeltaLoged deltaLoged)
        {
            if (deltaLoged.ReasonforCredithold == "Other") {
                deltaLoged.OtherReason = "Other-" + deltaLoged.OtherReason;
                deltaLoged.ReasonforCredithold = deltaLoged.OtherReason;
            }
            deltaLoged.CreatedBy = (int)Session["UserId"];
            deltaLoged.CreatedDate = DateTime.Now;
            deltaLoged.IsApproved = false;
            deltaLoged.LogedDataFor = "Credit Hold";

            if (deltaLoged.Id == 0)
                new DeltaLogedManager().CreateDeltaLog(deltaLoged);
            else
                new DeltaLogedManager().UpdateStatus(deltaLoged);

            return RedirectToAction("CreditHold", "CreditHold");
        }

        public JsonResult GetCreditHoldData()
        {
            bool IsAdmin = false;

            if (Session["Role"].ToString() == "5" || Session["Role"].ToString() == "4"|| Session["Role"].ToString() == "2")
                IsAdmin = true;
            var CreditHoldData = new DeltaLogedManager().GetDeltaLogByUser("Credit Hold", "0", (int)Session["UserId"], IsAdmin).OrderByDescending(o => o.CreditOnHold);
            return Json(CreditHoldData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGridDataByAPRep(string APRep,string DataFor)
         {  if (APRep != "")
            {
                var CreditHoldData = new DeltaLogedManager().GetLoggedGridDataByAPRep(DataFor, APRep,false).OrderByDescending(o => o.CreditOnHold);
                return Json(CreditHoldData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool IsAdmin = false;

                APRep = Session["UserId"].ToString();

                if (Session["Role"].ToString() == "5" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "2")
                    IsAdmin = true;

                var CreditHoldData = new DeltaLogedManager().GetLoggedGridDataByAPRep(DataFor, APRep, IsAdmin).OrderByDescending(o => o.CreditOnHold); 

                return Json(CreditHoldData, JsonRequestBehavior.AllowGet);
            }               
        }

        public JsonResult GetCreditHistoryHoldData()
        {
            bool IsAdmin = false;

            if (Session["Role"].ToString() == "5" || Session["Role"].ToString() == "4" || Session["Role"].ToString() == "2")
                IsAdmin = true;

            var CreditHoldData = new DeltaLogedManager().GetHistoryDeltaLogByUser("Credit Hold", Session["UserId"].ToString(), IsAdmin).OrderByDescending(o => o.CreditOnHold);
            return Json(CreditHoldData, JsonRequestBehavior.AllowGet);
        }

        // GET: CreditHoldStatus
        public ActionResult CreditHoldStatus()
        {
            return View();
        }

        // GET: CreditHoldStatus
        public ActionResult CreditHoldHistory()
        {
            return View();
        }

        public JsonResult GetAPUserData(string APRepId)
        {
            int Id = Convert.ToInt32(APRepId);
            var HeaderLogData = new DeltaLogedManager().GetCommonHeaderLogAPRepData(Id);

            return Json(HeaderLogData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAPRepUsersList()
        {
           return Json(new UserManager().GetUserList().Where(o => o.Role == "1").ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVendorList(string APRepId)
        {
            return Json(new VendorManager().GetVendorsByAPRep(Convert.ToInt32(APRepId)).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPODUserList()
        {
            return Json(new UserManager().GetUserList().Where(o => o.Role == "2").ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSupervisorList()
        {
            return Json(new UserManager().GetUserList().Where(o => o.Role == "4").ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPODList()
        {
            return Json(new UserManager().GetPODList().ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCreditHoldResonsList()
        {            
            return Json(new DeltaLogedManager().GetCreditHoldsReasons(), JsonRequestBehavior.AllowGet);
        }
    }
}