﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoney.Helpers;

namespace DeltaDashboard.Controllers
{
    [Authorization]
    public class InvoiceCheckListController : Controller
    {
        // GET: InvoiceCheckList
        public ActionResult Index()
        {
            return View();
        }

        // GET: SharedDrive
        public ActionResult SharedDrive()
        {
            return View();
        }

        // GET: CommonDrive
        public ActionResult CommonDrive()
        {
            return View();
        }
    }
}