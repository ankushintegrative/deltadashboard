﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeltaDashboard.Models
{
    public class VerificationViewModel
    {
        public int Index { get; set; }
        public int Status { get; set; }
        public bool Checked { get; set; }
    }
}