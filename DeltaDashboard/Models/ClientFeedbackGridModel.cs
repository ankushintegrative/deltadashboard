﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeltaDashboard.Models
{
    public class ClientFeedbackGridModel
    {
        public string ApRepName { get; set; }
        public int ApRepId { get; set; }
        public decimal Rating { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string RatingBy { get; set; }
        public string RatingDate { get; set; }
        public int UserId { get; set; }
        public string RoleId { get; set; }
    }
}