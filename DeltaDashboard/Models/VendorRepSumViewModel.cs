﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeltaDashboard.Models
{
    public class VendorRepSumViewModel
    {
        public string APRepName { get; set; }
        public string PODName { get; set; }
        public string Shift { get; set; }
        public string SupervisorName { get; set; }
        public int Escalations { get; set; }
        public int Credit { get; set; }
        public int Statements { get; set; }
        public int Assignment { get; set; }
    }
}