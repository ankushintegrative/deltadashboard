﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsMoneyCommon.HelperClass;

namespace TimeIsMoney.Helpers
{
    public class Authorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //for skip Session Check Action Filter
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipAuthorizeAttribute), false).Any())
            {
                return;
            }

            HttpContext ctx = HttpContext.Current;
            // check  sessions here
            if (HttpContext.Current.Session["UserId"] == null || HttpContext.Current.Session["UserName"] == null ||
                HttpContext.Current.Session["FullName"] == null || HttpContext.Current.Session["Role"] == null)
            {


                filterContext.Result = new RedirectResult("~/Account/SessionExpire");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class SkipAuthorizeAttribute : Attribute
    {
    }

    //To Delete All cache 
    public class NoCache : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();

            base.OnResultExecuting(filterContext);
        }
    }
}