﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class Vendor
    {
        public int Id { get; set; }
        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public bool? isStatement { get; set; }

        [NotMapped]
        public string StatementReq { get; set; }
    }
}
