﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class FeedbackTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Option { get; set; }
    }
}
