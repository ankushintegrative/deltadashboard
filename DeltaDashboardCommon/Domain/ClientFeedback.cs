﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class ClientFeedback
    {
        public int Id { get; set; }
        public int? ApRepId { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int? CSRType { get; set; }
        public int? FeedbackOption { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public string Comments { get; set; }
        [NotMapped]
        public List<FeedbackTypeViewModel> feedbackType { get; set; }

        [NotMapped]
        public string ApRepName { get; set; }

        [NotMapped]
        public string Rating { get; set; }

        [NotMapped]
        public int UserId { get; set; }

        [NotMapped]
        public string RoleId { get; set; }

    }
}
