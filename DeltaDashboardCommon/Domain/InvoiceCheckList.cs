﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class InvoiceCheckList
    {
        public int Id { get; set; }
        public string CheckList { get; set; }
        public string CheckListType { get; set; }
    }
}
