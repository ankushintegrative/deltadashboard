﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class Reasons
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public string ReasonType { get; set; }
    }
}
