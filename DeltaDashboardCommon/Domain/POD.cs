﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class POD
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [NotMapped]
        public string FullName { get; set; }
    }
}
