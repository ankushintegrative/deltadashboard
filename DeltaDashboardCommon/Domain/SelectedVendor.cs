﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class SelectedVendor
    {
        public int Id { get; set; }
        public string VendorName { get; set; }
    }
}
