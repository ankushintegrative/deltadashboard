﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class Reward
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string RewardTo { get; set; }
        public DateTime? RewardDate { get; set; }
        public string Comments { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        [NotMapped]
        public string RewardDateDisplay { get; set; }
    }
}
