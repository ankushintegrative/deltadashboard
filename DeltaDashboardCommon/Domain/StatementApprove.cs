﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class StatementApprove
    {
        public int Id { get; set; }
        public int? StatementId { get; set; }
        public bool? isRequested { get; set; }
        public DateTime? RequestedDate { get; set; }
        public int? RequestedBy { get; set; }
        public DateTime? RequestedApprovedDate { get; set; }

        public bool? isReceived { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public int? ReceivedBy { get; set; }
        public DateTime? ReceivedApprovedDate { get; set; }

        public bool? isMatchComplete { get; set; }
        public DateTime? MatchCompleteDate { get; set; }
        public int? MatchCompleteBy { get; set; }
        public DateTime? MatchCompleteApprovedDate { get; set; }

        public bool? isSettlementDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public int? SettlementBy { get; set; }
        public DateTime? SettlementApprovedDate { get; set; }

    }
}
