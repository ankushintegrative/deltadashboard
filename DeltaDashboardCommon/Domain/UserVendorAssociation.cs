﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
   public class UserVendorAssociation
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int VendorId { get; set; }
    }
}
