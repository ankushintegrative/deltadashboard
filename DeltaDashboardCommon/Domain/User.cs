﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeltaDashboardCommon.Domain
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string EmailId { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
        public int? ShiftId { get; set; }
        public int? PodId { get; set; }
        public int? SupervisorId { get; set; }
        public bool? isDeltaTeam { get; set; }

        [NotMapped]
        public string RoleName { get; set; }
        [NotMapped]
        public List<SelectedVendor> SelectedVendor { get; set; }
        [NotMapped]
        public bool IsValidUser { get; set; }
        [NotMapped]
        public string ErrorMsg { get; set; }

        [NotMapped]
        public string APRepName { get; set; }
        [NotMapped]
        public string SupervisorName { get; set; }
    }
}
