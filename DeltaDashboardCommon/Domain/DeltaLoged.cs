﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.Domain
{
    public class DeltaLoged
    {
        public int Id { get; set; }
        public string APRepID { get; set; }
        public int? VendorId { get; set; }
        public string ShiftId { get; set; }
        public string PODId { get; set; }
        public string SupervisorId { get; set; }
        public DateTime? CreditOnHold { get; set; }
        public DateTime? CreditOffHold { get; set; }
        public string DurationOfCreditHold { get; set; }
        public string ReasonforCredithold { get; set; }
        public string ActionPlanToPreventCredithold { get; set; }
        public string ReasonforEscalation { get; set; }
        public string ActionPlanToPreventEscalation { get; set; }
        public DateTime? StatementRequestedDate { get; set; }
        public DateTime? StatementReceivedDate { get; set; }
        public DateTime? StatementMatchCompleteDate { get; set; }
        public string LogedDataFor { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public string CurrentStatus { get; set; }

        [NotMapped]
        public string APRepName { get; set; }
        [NotMapped]
        public string AproverName { get; set; }

        [NotMapped]
        public string VendorName { get; set; }
        [NotMapped]
        public string VendorNumber { get; set; }
        [NotMapped]
        public string PODName { get; set; }
        [NotMapped]
        public string SupervisorName { get; set; }
        [NotMapped]
        public string DisplayCreditHoldOn { get; set; }
        [NotMapped]
        public string ShiftName { get; set; }
        [NotMapped]
        public string DisplayCreditHoldOff { get; set; }
        [NotMapped]
        public string DisplayCreatedDate { get; set; }

        [NotMapped]
        public string DisplayStatementRequestedDate { get; set; }

        [NotMapped]
        public string DisplayStatementReceivedDate { get; set; }

        [NotMapped]
        public string DisplayStatementMatchCompleteDate { get; set; }

        [NotMapped]
        public string DisplaySettlementDate { get; set; }

        [NotMapped]
        public string DisplayApprovedDate { get; set; }


        [NotMapped]
        public List<Reasons> ReasonList { get; set; }

        [NotMapped]
        public string OtherReason { get; set; }

        [NotMapped]
        public string message { get; set; }

        [NotMapped]
        public string StatusString { get; set; }

    }
}
