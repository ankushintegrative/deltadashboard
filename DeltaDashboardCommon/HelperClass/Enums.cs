﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeIsMoneyCommon.HelperClass
{
  public class Enums
    {
        public enum Role
        {
            APRepresentative = 1,
            PODLead = 2,
            TeamLead = 3,
            Supervisor = 4,
            Admin = 5,
            DataProcessing = 6
        } 
    }

    public static class stringhelper
    {
        public static string ShiftName(string ShiftId)
        {
            string ShiftName = string.Empty;
            switch (ShiftId)
            {
                case "1":
                    ShiftName = "Regular";
                    break;
                case "2":
                    ShiftName = "Night";
                    break;
                default:
                    break;
            }
            return ShiftName;
        }
    }
}
