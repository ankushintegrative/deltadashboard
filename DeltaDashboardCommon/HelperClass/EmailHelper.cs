﻿using System.Net.Mail;

namespace TimeIsMoneyCommon.HelperClass
{
    public static class EmailHelper
    {

        public static readonly string TheIndexFromEmailAddress = "itrs@integrative-systems.com";

        //TODO: write helper methods to build various email templates; use razor engine

        public static bool IsValidEmailAddress(string emailAddress)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(emailAddress))
                    return false;

                var mailAddress = new MailAddress(emailAddress);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
