﻿using System;
using System.Net;
using System.Net.Mail;

namespace TimeIsMoneyCommon.HelperClass
{

    public class Email
    {
        private static string smtpServer = "smtpServer"; //AppSettings.Get<string>("smtpServer");
        private static int smtpServerPort = 0; //AppSettings.Get<int>("smtpServerPort");
        private static string smtpUsername = "smtpServer"; //AppSettings.Get<string>("smtpUsername");
        private static string smptPassword = "smtpServer"; //AppSettings.Get<string>("smptPassword");

        private static string fromEmail = "smtpServerPort"; //AppSettings.Get<string>("fromEmail");
        private static string EmailSubject = "smtpServerPort"; //AppSettings.Get<string>("emailSubject");


        public static bool SendTimesheetSumbitEmail(string EmailPersonName, string ProjectName,string UserName, string EmailTo)
        {
            string Body = "<b>Hi " + EmailPersonName + ", <br /> Greeting from ITRS </b> <br /><br />";
            //Body = Body + "Timesheet has been submitted by " + UserName + ". <a href=\"http://integrativetimereporting.us-east-1.elasticbeanstalk.com/\" > Click Here  </a> to approve the timesheet . <br /><br /><br />";
            Body = Body + "Timesheet has been submitted by " + UserName + ". <a href=\"http://devitr.us-east-1.elasticbeanstalk.com/\" > Click Here  </a> to approve the timesheet . <br /><br /><br />";
            Body = Body + "Thank You <br /><b> ITRS </b> <br /> Integrative Time Reporting System";
            return SendEmail(fromEmail, EmailTo, EmailSubject, Body);
        }     

        public static bool SendEmail(string fromEmail, string toEmail, string emailSubject, string emailBody)
        {
            MailMessage mailMessage = new MailMessage();

            try
            {
                // Outside of the application the "fromEmail" must be Verified on AWS Simple Mail Service in order for the SendEmail to work. 
                MailAddress fromMail = new MailAddress(fromEmail);
                mailMessage.To.Add(toEmail);

                mailMessage.From = fromMail;
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = emailSubject;
                mailMessage.Body = emailBody;

                if (mailMessage != null && !EmailHelper.IsValidEmailAddress(mailMessage.To.ToString()))
                    throw new FormatException("Invalid Email Address");

                using (var smtpClient = new SmtpClient(smtpServer, smtpServerPort))
                {
                    smtpClient.Credentials = new NetworkCredential(smtpUsername, smptPassword);
                    //smtpClient.UseDefaultCredentials = false;
                    smtpClient.EnableSsl = true;

                    using (mailMessage)
                    {
                        smtpClient.Send(mailMessage);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {               
                return false;
            }
        }
    }
}


