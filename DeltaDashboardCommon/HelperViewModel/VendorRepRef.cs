﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.HelperViewModel
{
   public class VendorRepRef
    {
        public string APRepName { get; set; }
        public string VendorName { get; set; }
        public string VendorNumber { get; set; }
    }
}
