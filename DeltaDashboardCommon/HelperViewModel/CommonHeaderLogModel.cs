﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.HelperViewModel
{
   public class CommonHeaderLogModel
    {
     
        public int? ShiftId { get; set; }
        public string ShiftName { get; set; }
        public int? PODId { get; set; }
        public string PODName { get; set; }
        public int? SupervisorId { get; set; }
        public string SupervisorName { get; set; }
    }
}
