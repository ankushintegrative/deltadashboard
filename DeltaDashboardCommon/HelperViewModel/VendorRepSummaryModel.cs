﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.HelperViewModel
{
    public class VendorRepSummaryModel
    {
        public int APRepID { get; set; }
        public string APRepName { get; set; }
        public string PODName { get; set; }
        public string Shift { get; set; }
        public string ShiftId { get; set; }
        public string SupervisorName { get; set; }
        public string Escalations { get; set; }
        public string Credit { get; set; }
        public string Statements { get; set; }
        public string Assignment { get; set; }
    }
}
