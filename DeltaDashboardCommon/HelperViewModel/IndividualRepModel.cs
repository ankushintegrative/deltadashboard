﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardCommon.HelperViewModel
{
    public class IndividualRepModel
    {
        public string VendorName { get; set; }
        public string VendorNumber { get; set; }
        public string StatementRequestedDate { get; set; }
        public string Past6MonthEscalations { get; set; }
        public string Past6MonthCredit { get; set; }
        public string SettlementDate { get; set; }
    }
}
