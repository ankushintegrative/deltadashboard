﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DeltaDashboardCommon.Domain;
namespace DeltaDashboardDAL
{
    public class DeltaContext : DbContext
    {
        public DeltaContext()
            : base("DeltaConnetion")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Vendor> Vendor { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<UserVendorAssociation> UserVendorAssociation { get; set; }
        public virtual DbSet<POD> POD { get; set; }
        public virtual DbSet<Reasons> Reasons { get; set; }
        public virtual DbSet<DeltaLoged> DeltaLoged { get; set; }
        public virtual DbSet<InvoiceCheckList> InvoiceCheckList { get;set;}
        public virtual DbSet<ClientFeedback> ClientFeedback { get; set; }
        public virtual DbSet<StatementApprove> StatementApprove { get; set; }

        //Added by Shraddha on 16-Jun-2022
        public virtual DbSet<Reward> Reward { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DeltaContext>(null);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
