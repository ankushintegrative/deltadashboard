﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeltaDashboardCommon.Domain;

namespace DeltaDashboardDAL.Repositories.POD
{
    interface IPODRepository : IRepository<DeltaDashboardCommon.Domain.POD>
    {
    }
}
