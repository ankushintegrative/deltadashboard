﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.POD
{
    class PODRepository : Repository<DeltaDashboardCommon.Domain.POD>, IPODRepository
    {
        public PODRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }
    }
}
