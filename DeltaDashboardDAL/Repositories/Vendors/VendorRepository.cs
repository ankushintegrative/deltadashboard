﻿using DeltaDashboardCommon.Domain;
using System.Collections.Generic;
using System.Linq;

namespace DeltaDashboardDAL.Repositories.Vendors
{
    class VendorRepository : Repository<Vendor>, IVendorRepository
    {
        public VendorRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }

        public Vendor GetVendorByNumber(string number)
        {
            var vendor = DeltaContext.Vendor
                .Where(c => c.VendorNumber == number)
                .FirstOrDefault();

            return vendor;
        }
    }
}
