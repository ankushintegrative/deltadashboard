﻿using DeltaDashboardCommon.Domain;
using System.Collections.Generic;

namespace DeltaDashboardDAL.Repositories.Vendors
{
    interface IVendorRepository : IRepository<Vendor>
    {
        Vendor GetVendorByNumber(string number);
    }
}
