﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.DeltaLogeds
{
    class DeltaLogedRepository:Repository<DeltaLoged>, IDeltaLogedRepository
    {
        public DeltaLogedRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }
    }
}
