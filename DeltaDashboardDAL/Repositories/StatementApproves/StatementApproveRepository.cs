﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.StatementApproves
{
    class StatementApproveRepository : Repository<StatementApprove>, IStatementApproveRepository
    {
        public StatementApproveRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }

        public StatementApprove GetDataByStatementId(int id)
        {
            var statementApprove = DeltaContext.StatementApprove
                .Where(c => c.StatementId == id).FirstOrDefault();

            return statementApprove;
        }
    }
}
