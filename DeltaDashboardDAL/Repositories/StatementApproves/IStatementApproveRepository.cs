﻿using DeltaDashboardCommon.Domain;
using System.Collections.Generic;

namespace DeltaDashboardDAL.Repositories.StatementApproves
{
    interface IStatementApproveRepository : IRepository<StatementApprove>
    {
        StatementApprove GetDataByStatementId(int id);
    }
}
