﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.InvoiceCheckLists
{
    interface IInvoiceCheckListRepository:IRepository<InvoiceCheckList>
    {
    }
}
