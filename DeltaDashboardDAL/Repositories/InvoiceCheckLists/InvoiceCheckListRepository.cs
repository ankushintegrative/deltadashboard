﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.InvoiceCheckLists
{
    class InvoiceCheckListRepository:Repository<InvoiceCheckList>, IInvoiceCheckListRepository
    {
        public InvoiceCheckListRepository(DeltaContext deltaContext) : base(deltaContext)
    { }

    public DeltaContext DeltaContext
    {
        get { return Context as DeltaContext; }
    }

  
}
}
