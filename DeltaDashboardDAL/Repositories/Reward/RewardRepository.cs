﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.Reward
{
    class RewardRepository : Repository<DeltaDashboardCommon.Domain.Reward>, IRewardRepository
    {
        public RewardRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }
    }
}
