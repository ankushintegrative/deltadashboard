﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.Reward
{
    interface IRewardRepository :IRepository<DeltaDashboardCommon.Domain.Reward>
    {
    }
}
