﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.ClientFeedbacks
{
    class ClientFeedbackRepository : Repository<ClientFeedback>, IClientFeedbackRepository
    {
        public ClientFeedbackRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }

        public List<ClientFeedback> GetDataByApRep(int aprepId, string month, string year)
        {
            var clientFeedback = DeltaContext.ClientFeedback
                .Where(c => c.ApRepId == aprepId && c.Month == month && c.Year == year).ToList();

            return clientFeedback;
        }
    }
}
