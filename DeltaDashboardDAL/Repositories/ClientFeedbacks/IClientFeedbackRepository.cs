﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.ClientFeedbacks
{
    interface IClientFeedbackRepository : IRepository<ClientFeedback>
    {
        List<ClientFeedback> GetDataByApRep(int aprepId, string month, string year);
    }
}
