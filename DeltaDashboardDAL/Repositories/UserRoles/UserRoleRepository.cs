﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.UserRoles
{
    class UserRoleRepository:Repository<UserRole>,IUserRoleRepository
    {
        public UserRoleRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }      
    }
}
