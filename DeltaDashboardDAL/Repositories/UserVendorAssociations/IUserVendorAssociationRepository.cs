﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeltaDashboardCommon.Domain;

namespace DeltaDashboardDAL.Repositories.UserVendorAssociations
{
    interface IUserVendorAssociationRepository : IRepository<UserVendorAssociation>
    {
    }
}
