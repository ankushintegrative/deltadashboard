﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Repositories.UserVendorAssociations
{
    class UserVendorAssociationRepository : Repository<UserVendorAssociation>,IUserVendorAssociationRepository
    {
        public UserVendorAssociationRepository(DeltaContext deltaContext) : base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }

    }
}
