﻿using System.Linq;
using DeltaDashboardCommon.Domain;

namespace DeltaDashboardDAL.Repositories.Users
{
   class UserRepository:Repository<User>, IUserRepository
   {
        public UserRepository(DeltaContext deltaContext) :base(deltaContext)
        { }

        public DeltaContext DeltaContext
        {
            get { return Context as DeltaContext; }
        }

        public User GetUserByUserName(string userName)
        {
            var user = DeltaContext.User
                .Where(c => c.UserName == userName)
                .FirstOrDefault();

            return user;
        }
   }
}
