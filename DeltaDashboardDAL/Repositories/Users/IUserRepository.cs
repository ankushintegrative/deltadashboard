﻿using DeltaDashboardCommon.Domain;

namespace DeltaDashboardDAL.Repositories.Users
{
    interface IUserRepository:IRepository<User>
    {
        User GetUserByUserName(string userName);
    }
}
