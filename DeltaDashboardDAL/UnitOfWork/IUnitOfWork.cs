﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.UnitOfWork
{
    interface IUnitOfWork:IDisposable
    {        
        Repositories.Users.IUserRepository User { get; }
        Repositories.UserRoles.IUserRoleRepository UserRole { get; }
        Repositories.Vendors.IVendorRepository Vendor { get; }
        Repositories.UserVendorAssociations.IUserVendorAssociationRepository UserVendorAssociation { get; }     
        Repositories.DeltaLogeds.IDeltaLogedRepository DeltaLoged { get; }
        Repositories.InvoiceCheckLists.IInvoiceCheckListRepository InvoiceCheckList { get; }
        Repositories.ClientFeedbacks.IClientFeedbackRepository ClientFeedback { get; }
        Repositories.POD.IPODRepository POD { get; }
        Repositories.StatementApproves.IStatementApproveRepository StatementApprove { get; }
    }
}
