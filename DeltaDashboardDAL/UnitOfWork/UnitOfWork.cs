﻿
using System;

namespace DeltaDashboardDAL.UnitOfWork
{
    class UnitOfWork : IUnitOfWork
    {
        private readonly DeltaContext _context;
        public UnitOfWork(DeltaContext deltaContext)
        {
            _context = deltaContext;
            User = new Repositories.Users.UserRepository(_context);
            Vendor = new Repositories.Vendors.VendorRepository(_context);     
            UserRole = new Repositories.UserRoles.UserRoleRepository(_context);
            UserVendorAssociation = new Repositories.UserVendorAssociations.UserVendorAssociationRepository(_context);
            InvoiceCheckList = new Repositories.InvoiceCheckLists.InvoiceCheckListRepository(_context);
            DeltaLoged = new Repositories.DeltaLogeds.DeltaLogedRepository(_context);
            POD = new Repositories.POD.PODRepository(_context);
            ClientFeedback = new Repositories.ClientFeedbacks.ClientFeedbackRepository(_context);
            StatementApprove = new Repositories.StatementApproves.StatementApproveRepository(_context);
            // Added by Shraddha on 16-Jun-2022
            Reward = new Repositories.Reward.RewardRepository(_context);
        }

        public Repositories.Users.IUserRepository User { get; private set; }
        public Repositories.Vendors.IVendorRepository Vendor { get; private set; }
        public Repositories.UserRoles.IUserRoleRepository UserRole { get; private set; }
        public Repositories.UserVendorAssociations.IUserVendorAssociationRepository UserVendorAssociation { get; private set; }        
        public Repositories.DeltaLogeds.IDeltaLogedRepository DeltaLoged { get; private set; }
        public Repositories.POD.IPODRepository POD { get; private set; }
        public Repositories.InvoiceCheckLists.IInvoiceCheckListRepository InvoiceCheckList { get; private set; }
        public Repositories.ClientFeedbacks.IClientFeedbackRepository ClientFeedback { get; private set; }
        public Repositories.StatementApproves.IStatementApproveRepository StatementApprove { get; private set; }

        // Added by Shraddha on 16-Jun-2022
        public Repositories.Reward.IRewardRepository Reward { get; private set; }
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            if (_context != null)
                _context.Dispose();
        }
    }
}
