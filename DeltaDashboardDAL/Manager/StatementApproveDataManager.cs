﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Manager
{
    public class StatementApproveDataManager
    {
        private DeltaContext context = new DeltaContext();
        UnitOfWork.UnitOfWork unitOfWork = null;

        public StatementApproveDataManager()
        {
            unitOfWork = new UnitOfWork.UnitOfWork(context);
        }

        public void CreateStatementApprove(StatementApprove statementApprove)
        {
            unitOfWork.StatementApprove.Add(statementApprove);
            unitOfWork.Complete();
        }

        public StatementApprove UpdateStatementApprove(StatementApprove statementApprove)
        {
            statementApprove = unitOfWork.StatementApprove.Update(statementApprove);
            unitOfWork.Complete();
            return statementApprove;
        }

        public StatementApprove GetDataByStatementId(int id)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.StatementApprove.GetDataByStatementId(id);
        }
    }
}
