﻿using DeltaDashboardCommon.Domain;
using DeltaDashboardCommon.HelperViewModel;
using DeltaDashboardDAL.Repositories.DeltaLogeds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeIsMoneyCommon.HelperClass;

namespace DeltaDashboardDAL.Manager
{
    public class DeltaLogedDataManager
    {
        private DeltaContext context = new DeltaContext();

        UnitOfWork.UnitOfWork unitOfWork = null; 

        public DeltaLogedDataManager()
        {
            unitOfWork = new UnitOfWork.UnitOfWork(context);
        }
        
        public int CreateDeltaLog(DeltaLoged deltaLoged)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            unitOfWork.DeltaLoged.Add(deltaLoged);
            unitOfWork.Complete();
            return deltaLoged.Id;
        }

        public User UpdateUser(User user)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            user = unitOfWork.User.Update(user);
            unitOfWork.Complete();
            return user;
        }

        public DeltaLoged UpdateStatus(DeltaLoged deltaLoged)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            deltaLoged = unitOfWork.DeltaLoged.Update(deltaLoged);
            unitOfWork.Complete();
            return deltaLoged;
        }

        public List<DeltaLoged> GetNotApprovedDeltaLog(string LogType, string Status)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            List<DeltaLoged> deltaLogs = new List<DeltaLoged>();
            if (LogType == "" && Status == "")
            {
                deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor != "Credit Hold").ToList();
                deltaLogs.AddRange(unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == "Credit Hold" && o.CreditOnHold != null && o.CreditOffHold != null).ToList());
            }
            else if (LogType == "" && Status != "")
            {
                deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor != "Credit Hold").ToList();

                deltaLogs.AddRange(unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == "Credit Hold" && o.CreditOnHold != null && o.CreditOffHold != null).ToList());
            }
            else if (LogType != "" && Status == "")
            {
                if (LogType == "Credit Hold")
                {
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType && o.CreditOnHold != null && o.CreditOffHold != null).ToList();
                }
                else
                {
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType).ToList();
                }
            }
            else
            {
                if (LogType == "Credit Hold")
                {
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == LogType && o.CreditOnHold != null && o.CreditOffHold != null).ToList();
                }
                else
                {
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == LogType).ToList();
                }
            }

            var Creditholdlist = (from DL in deltaLogs
                                  join
                                  U in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.APRepID) equals U.Id
                                  join
                                  V in unitOfWork.Vendor.GetAll()
                                  on DL.VendorId equals V.Id
                                  join
                                  S in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.SupervisorId) equals S.Id
                                  join
                                  UP in unitOfWork.POD.GetAll()
                                  on DL.PODId equals Convert.ToString(UP.Id)

                                  select new DeltaLoged
                                  {
                                      Id = DL.Id,
                                      LogedDataFor = DL.LogedDataFor,
                                      PODName = UP.Name,
                                      APRepID = DL.APRepID,
                                      APRepName = U.FullName,
                                      VendorName = V.VendorName,
                                      VendorNumber = V.VendorNumber,
                                      SupervisorName = S.FullName,
                                      ApprovedDate = DL.ApprovedDate,
                                      DurationOfCreditHold = DL.DurationOfCreditHold != null ? DL.DurationOfCreditHold : DL.CreditOffHold != null ? DL.DurationOfCreditHold : Convert.ToString(Convert.ToDateTime(DL.CreditOnHold).Subtract(DateTime.Now)),
                                      ReasonforCredithold = DL.ReasonforCredithold,
                                      ShiftId = DL.ShiftId,
                                      CreditOnHold = DL.CreditOnHold,
                                      CreditOffHold = DL.CreditOffHold,
                                      DisplayCreditHoldOn = DL.CreditOnHold != null ? Convert.ToDateTime(DL.CreditOnHold).ToString("MM/dd/yy") : "",
                                      DisplayCreditHoldOff = DL.CreditOffHold != null ? Convert.ToDateTime(DL.CreditOffHold).ToString("MM/dd/yy") : "",
                                      ActionPlanToPreventCredithold = DL.ActionPlanToPreventCredithold,
                                      ReasonforEscalation = DL.ReasonforEscalation,
                                      ActionPlanToPreventEscalation = DL.ActionPlanToPreventEscalation,
                                      DisplayStatementRequestedDate = DL.StatementRequestedDate != null ? Convert.ToDateTime(DL.StatementRequestedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementReceivedDate = DL.StatementReceivedDate != null ? Convert.ToDateTime(DL.StatementReceivedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementMatchCompleteDate = DL.StatementMatchCompleteDate != null ? Convert.ToDateTime(DL.StatementMatchCompleteDate).ToString("MM/dd/yy") : "",
                                      IsApproved = DL.IsApproved,
                                      ApprovedBy = DL.ApprovedBy,
                                      CreatedDate = DL.CreatedDate,
                                      DisplayCreatedDate = Convert.ToDateTime(DL.CreatedDate).ToString("MM/dd/yy")

                                  }).ToList();

            return Creditholdlist;
        }

        public List<DeltaLoged> GetDataByChangeStatus(bool Status)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var changeStatusList = (from DL in unitOfWork.DeltaLoged.GetAll().Where(U => U.IsApproved == Status)
                                    join
                                    U in unitOfWork.User.GetAll()
                                    on Convert.ToInt32(DL.APRepID) equals U.Id
                                    join
                                    V in unitOfWork.Vendor.GetAll()
                                    on DL.VendorId equals V.Id
                                    join
                                    S in unitOfWork.User.GetAll()
                                    on Convert.ToInt32(DL.SupervisorId) equals S.Id
                                    join
                                    UP in unitOfWork.POD.GetAll()
                                    on DL.PODId equals Convert.ToString(UP.Id)

                                    select new DeltaLoged
                                    {
                                        Id = DL.Id,
                                        LogedDataFor = DL.LogedDataFor,
                                        PODName = UP.Name,
                                        APRepID = DL.APRepID,
                                        APRepName = U.FullName,
                                        VendorName = V.VendorName,
                                        VendorNumber = V.VendorNumber,
                                        SupervisorName = S.FullName,
                                        ApprovedDate = DL.ApprovedDate,
                                        DurationOfCreditHold = DL.DurationOfCreditHold,
                                        ReasonforCredithold = DL.ReasonforCredithold,
                                        ShiftId = DL.ShiftId,
                                        CreditOnHold = DL.CreditOnHold,
                                        CreditOffHold = DL.CreditOffHold,
                                        DisplayCreditHoldOn = DL.CreditOnHold != null ? Convert.ToDateTime(DL.CreditOnHold).ToString("MM/dd/yy") : "",
                                        DisplayCreditHoldOff = DL.CreditOffHold != null ? Convert.ToDateTime(DL.CreditOffHold).ToString("MM/dd/yy") : "",
                                        ActionPlanToPreventCredithold = DL.ActionPlanToPreventCredithold,
                                        ReasonforEscalation = DL.ReasonforEscalation,
                                        ActionPlanToPreventEscalation = DL.ActionPlanToPreventEscalation,
                                        DisplayStatementRequestedDate = DL.StatementRequestedDate != null ? Convert.ToDateTime(DL.StatementRequestedDate).ToString("MM/dd/yy") : "",
                                        DisplayStatementReceivedDate = DL.StatementReceivedDate != null ? Convert.ToDateTime(DL.StatementReceivedDate).ToString("MM/dd/yy") : "",
                                        DisplayStatementMatchCompleteDate = DL.StatementMatchCompleteDate != null ? Convert.ToDateTime(DL.StatementMatchCompleteDate).ToString("MM/dd/yy") : "",
                                        DisplaySettlementDate = DL.SettlementDate != null ? Convert.ToDateTime(DL.SettlementDate).ToString("MM/dd/yy") : "",
                                        IsApproved = DL.IsApproved,
                                        ApprovedBy = DL.ApprovedBy,
                                        CreatedDate = DL.CreatedDate,
                                        DisplayCreatedDate = Convert.ToDateTime(DL.CreatedDate).ToString("MM/dd/yy")

                                    }).ToList();

            return changeStatusList;
        }

        public DeltaLoged GetDataByChangeStatusDetails(int id)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.DeltaLoged.Get(id);
        }

        public CommonHeaderLogModel GetCommonHeaderLogAPRepData(int APRepID)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);

            var UserDetail = unitOfWork.User.GetAll().Where(o => o.Id == APRepID).ToList();
            var HeaderLogModel = (from U in UserDetail
                                  join
                                  UP in unitOfWork.POD.GetAll()
                                  on U.PodId equals UP.Id
                                  join
                                  US in unitOfWork.User.GetAll()
                                  on U.SupervisorId equals US.Id

                                  select new CommonHeaderLogModel
                                  {
                                      PODId = U.PodId,
                                      PODName = UP.Name,
                                      SupervisorId = U.SupervisorId,
                                      SupervisorName = US.FullName,
                                      ShiftId = U.ShiftId,
                                      ShiftName = U.ShiftId == 1 ? "Regular" : "Night"
                                  }
                                ).FirstOrDefault();

            return HeaderLogModel;
        }

        /// <summary>
        /// Calling same method to get data for Credit Hold and Escalation Grid on Dashboard and Keeping the Status ID for Future referance (still not clear about what status type data should display on grid. now we fetching both pending & Approved data on grid.
        /// </summary>
        /// <param name="LogType"></param>
        /// <param name="Status"></param>
        /// <param name="CreatedBy"></param>
        /// <returns></returns>
        public List<DeltaLoged> GetDeltaLogByUser(string LogType, string Status, int CreatedBy, bool IsAdmin)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            List<DeltaLoged> deltaLogs = new List<DeltaLoged>();

            if (LogType == "" && Status == "")
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.CreatedBy == CreatedBy).ToList();
            }
            else if (LogType == "" && Status != "")
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false)).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.CreatedBy == CreatedBy).ToList();
            }
            else if (LogType != "" && Status == "")
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType && o.CreatedBy == CreatedBy).ToList();
            }
            else
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == LogType).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == LogType && o.CreatedBy == CreatedBy).ToList();
            }

            var Creditholdlist = (from DL in deltaLogs
                                  join
                                  U in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.APRepID) equals U.Id
                                  join
                                  V in unitOfWork.Vendor.GetAll()
                                  on DL.VendorId equals V.Id
                                  join
                                  S in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.SupervisorId) equals S.Id
                                  join
                                  UP in unitOfWork.POD.GetAll()
                                  on DL.PODId equals Convert.ToString(UP.Id)

                                  select new DeltaLoged
                                  {
                                      Id = DL.Id,
                                      LogedDataFor = DL.LogedDataFor,
                                      PODId = DL.PODId,
                                      PODName = UP.Name,
                                      APRepID = DL.APRepID,
                                      APRepName = U.FullName,
                                      VendorId = V.Id,
                                      VendorName = V.VendorName,
                                      VendorNumber = V.VendorNumber,
                                      SupervisorId = DL.SupervisorId,
                                      SupervisorName = S.FullName,
                                      ApprovedDate = DL.ApprovedDate,

                                      DurationOfCreditHold = DL.DurationOfCreditHold != null ? DL.DurationOfCreditHold : DL.CreditOffHold != null ?
                                      DL.DurationOfCreditHold :
                                      Convert.ToString(-(Convert.ToDateTime(DL.CreditOnHold).Subtract(DateTime.Now).Days)),

                                      ReasonforCredithold = DL.ReasonforCredithold,
                                      ShiftId = DL.ShiftId,
                                      ShiftName = DL.ShiftId == "1" ? "Regular" : "Night",
                                      CreditOnHold = DL.CreditOnHold,
                                      CreditOffHold = DL.CreditOffHold,
                                      DisplayCreditHoldOn = DL.CreditOnHold != null ? Convert.ToDateTime(DL.CreditOnHold).ToString("MM/dd/yy") : "",
                                      DisplayCreditHoldOff = DL.CreditOffHold != null ? Convert.ToDateTime(DL.CreditOffHold).ToString("MM/dd/yy") : "",
                                      ActionPlanToPreventCredithold = DL.ActionPlanToPreventCredithold,
                                      ReasonforEscalation = DL.ReasonforEscalation,
                                      ActionPlanToPreventEscalation = DL.ActionPlanToPreventEscalation,
                                      DisplayStatementRequestedDate = DL.StatementRequestedDate != null ? Convert.ToDateTime(DL.StatementRequestedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementReceivedDate = DL.StatementReceivedDate != null ? Convert.ToDateTime(DL.StatementReceivedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementMatchCompleteDate = DL.StatementMatchCompleteDate != null ? Convert.ToDateTime(DL.StatementMatchCompleteDate).ToString("MM/dd/yy") : "",
                                      DisplaySettlementDate = DL.SettlementDate != null ? Convert.ToDateTime(DL.SettlementDate).ToString("MM/dd/yy") : "",
                                      IsApproved = DL.IsApproved,
                                      DisplayApprovedDate = DL.ApprovedDate != null ? Convert.ToDateTime(DL.ApprovedDate).ToString("MM/dd/yy") : "",
                                      ApprovedBy = DL.ApprovedBy,

                                      CreatedDate = DL.CreatedDate,
                                      DisplayCreatedDate = Convert.ToDateTime(DL.CreatedDate).ToString("MM/dd/yy"),
                                      CurrentStatus = DL.CurrentStatus

                                  }).ToList();

            return Creditholdlist;
        }

        public List<IndividualRepModel> GetIndividualRep(string APRepId)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var APRep = Convert.ToInt32(APRepId);
            var vendorList = new VendorDataManager().GetVendorsByIndividualAPRep(APRep);
            List<IndividualRepModel> IndividualRepModelList = new List<IndividualRepModel>();
            DateTime SixMonthOldDate = DateTime.Now.AddMonths(-6);
            foreach (var v in vendorList)
            {
                IndividualRepModel individualRep = new IndividualRepModel();
                individualRep.VendorName = v.VendorName;
                individualRep.VendorNumber = v.VendorNumber;
                individualRep.Past6MonthCredit = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == APRepId && o.VendorId == v.Id && o.CreditOnHold >= SixMonthOldDate && o.LogedDataFor == "Credit Hold").Count().ToString();
                individualRep.Past6MonthCredit = individualRep.Past6MonthCredit == "0" ? "" : individualRep.Past6MonthCredit;

                individualRep.Past6MonthEscalations = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == APRepId && o.VendorId == v.Id && o.CreatedDate >= SixMonthOldDate && o.LogedDataFor == "Escalation").Count().ToString();
                individualRep.Past6MonthEscalations = individualRep.Past6MonthEscalations == "0" ? "" : individualRep.Past6MonthEscalations;

                var ReqDate = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == APRepId && o.VendorId == v.Id && o.LogedDataFor == "Statement Receipt").OrderByDescending(o => o.StatementReceivedDate).Select(o => o.StatementReceivedDate).FirstOrDefault();

                individualRep.StatementRequestedDate = ReqDate != null ? Convert.ToDateTime(ReqDate).ToString("MM/dd/yy") : "";

                var SettlementDate = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == APRepId && o.VendorId == v.Id && o.LogedDataFor == "Statement Receipt" && o.SettlementDate != null).OrderByDescending(o => o.SettlementDate).Select(o => o.SettlementDate).FirstOrDefault();

                individualRep.SettlementDate = SettlementDate != null ? Convert.ToDateTime(SettlementDate).ToString("MM/dd/yy") : "";

                IndividualRepModelList.Add(individualRep);

            }
            return IndividualRepModelList;
        }

        public List<VendorRepSummaryModel> GetVendorRepSummary()
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            List<VendorRepSummaryModel> vendorRepSummaries = new List<VendorRepSummaryModel>();
            var vendorReplist = (from Users in unitOfWork.User.GetAll().Where(o => o.Role == "1" || o.Role == "6")
                                 join
                                 pod in unitOfWork.POD.GetAll()
                                 on Convert.ToInt32(Users.PodId) equals pod.Id
                                 join
                                 supervisor in unitOfWork.User.GetAll()
                                 on Users.SupervisorId equals supervisor.Id

                                 select new VendorRepSummaryModel
                                 {
                                     APRepID = Users.Id,
                                     PODName = pod.Name,
                                     APRepName = Users.FullName,
                                     SupervisorName = supervisor.FullName,
                                     ShiftId = stringhelper.ShiftName(Convert.ToString(Users.ShiftId))
                                 }).ToList();


            DateTime Previouse30DaysDate = DateTime.Now.AddDays(-30);
            foreach (var VR in vendorReplist)
            {
                VendorRepSummaryModel vendorRep = new VendorRepSummaryModel();

                vendorRep.APRepName = VR.APRepName;
                vendorRep.PODName = VR.PODName;
                vendorRep.SupervisorName = VR.SupervisorName;
                vendorRep.ShiftId = VR.ShiftId;
                vendorRep.Escalations = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == Convert.ToString(VR.APRepID) && o.LogedDataFor == "Escalation" && o.CreatedDate >= Previouse30DaysDate).Count().ToString();
                vendorRep.Escalations = vendorRep.Escalations == "0" ? "" : vendorRep.Escalations;
                vendorRep.Credit = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == Convert.ToString(VR.APRepID) && o.LogedDataFor == "Credit Hold" && o.CreditOffHold == null).Count().ToString();
                vendorRep.Credit = vendorRep.Credit == "0" ? "" : vendorRep.Credit;
                var UAV = new UserVendorAssociationDataManager().GetAssociationVendor(VR.APRepID);
                vendorRep.Assignment = UAV.Count().ToString();
                vendorRep.Assignment = vendorRep.Assignment == "0" ? "" : vendorRep.Assignment;
                /*             vendorRep.Statements*/
                var StatmentRequiredCont = (from UV in UAV
                                            join
                                            V in unitOfWork.Vendor.GetAll()
                                            on UV.VendorId equals V.Id
                                            select new Vendor
                                            {
                                                Id = V.Id,
                                                isStatement = V.isStatement

                                            }).Where(o => o.isStatement == true).Count();

                var StatmentReceivedCount = (from SRC in unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == Convert.ToString(VR.APRepID) && o.LogedDataFor == "Statement Receipt" && (o.IsApproved == null || o.IsApproved == true) && o.StatementReceivedDate != null && (o.StatementRequestedDate.Value.Month == DateTime.Now.Month && o.StatementRequestedDate.Value.Year == DateTime.Now.Year)).Select(o=>o.VendorId).Distinct()
                                             join
                                              V in unitOfWork.Vendor.GetAll()
                                         on  Convert.ToInt32(SRC.Value) equals V.Id
                                             select new Vendor
                                             {
                                                 Id = V.Id,
                                                 isStatement = V.isStatement

                                             }).Where(o => o.isStatement == true).Count();

              
                vendorRep.Statements = (StatmentRequiredCont - StatmentReceivedCount).ToString();

                vendorRep.Statements = vendorRep.Statements == "0" ? "" : vendorRep.Statements;

                vendorRepSummaries.Add(vendorRep);

            }
            return vendorRepSummaries;
        }

        public List<VendorRepRef> GetVendorRepRef(string VendorId, string APRepId)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);

            List<UserVendorAssociation> userVendorList = new List<UserVendorAssociation>();
            if (VendorId == "" && APRepId == "")
            {
                userVendorList = unitOfWork.UserVendorAssociation.GetAll().ToList();
            }
            else if (VendorId == "" && APRepId != "")
            {
                userVendorList = unitOfWork.UserVendorAssociation.GetAll().Where(o => o.UserId == Convert.ToInt32(APRepId)).ToList();
            }
            else if (VendorId != "" && APRepId == "")
            {
                userVendorList = unitOfWork.UserVendorAssociation.GetAll().Where(o => o.VendorId == Convert.ToInt32(VendorId)).ToList();
            }
            else
            {
                unitOfWork.UserVendorAssociation.GetAll().Where(o => o.UserId == Convert.ToInt32(APRepId) && o.VendorId == Convert.ToInt32(VendorId)).ToList();
            }

            var vendorRepRef = (from VU in userVendorList
                                join
                                Rep in unitOfWork.User.GetAll()
                                on
                                VU.UserId equals Rep.Id
                                join
                                vendor in unitOfWork.Vendor.GetAll()
                                on
                                VU.VendorId equals vendor.Id

                                select new VendorRepRef
                                {
                                    VendorName = vendor.VendorName,
                                    APRepName = Rep.FullName,
                                    VendorNumber = vendor.VendorNumber
                                }).ToList();

            return vendorRepRef;
        }

        public List<Reasons> GetCreditHoldsReasons()
        {
            return context.Reasons.ToList().Where(o => o.ReasonType == "Credit Hold").OrderBy(o => o.Reason).ToList();
        }

        public List<Reasons> GetEscalationsReasons()
        {
            return context.Reasons.ToList().Where(o => o.ReasonType == "Escalation").OrderBy(o => o.Reason).ToList();
        }

        public List<DeltaLoged> GetLoggedGridDataByAPRep(string LogType, string APrep, bool IsAdmin)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            List<DeltaLoged> deltaLogs = new List<DeltaLoged>();

            if (IsAdmin)
            {
                if (LogType != "Statement Receipt")
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == false && o.LogedDataFor == LogType).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType && (o.StatementRequestedDate == null || o.StatementReceivedDate == null || o.StatementMatchCompleteDate == null || o.SettlementDate == null)).OrderByDescending(o => o.StatementRequestedDate).ToList();
            }
            else
            {
                if (LogType != "Statement Receipt")
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == false && o.LogedDataFor == LogType && o.APRepID == APrep).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType && o.APRepID == APrep && (o.StatementRequestedDate == null || o.StatementReceivedDate == null || o.StatementMatchCompleteDate == null || o.SettlementDate == null)).OrderByDescending(o => o.StatementRequestedDate).ToList();
            }

            var deltaLogslist = (from DL in deltaLogs
                                 join
                                 U in unitOfWork.User.GetAll()
                                 on Convert.ToInt32(DL.APRepID) equals U.Id
                                 join
                                 V in unitOfWork.Vendor.GetAll()
                                 on DL.VendorId equals V.Id
                                 join
                                 S in unitOfWork.User.GetAll()
                                 on Convert.ToInt32(DL.SupervisorId) equals S.Id
                                 join
                                 UP in unitOfWork.POD.GetAll()
                                 on DL.PODId equals Convert.ToString(UP.Id)

                                 select new DeltaLoged
                                 {
                                     Id = DL.Id,
                                     LogedDataFor = DL.LogedDataFor,
                                     PODId = DL.PODId,
                                     PODName = UP.Name,
                                     APRepID = DL.APRepID,
                                     APRepName = U.FullName,
                                     VendorId = V.Id,
                                     VendorName = V.VendorName,
                                     VendorNumber = V.VendorNumber,
                                     SupervisorId = DL.SupervisorId,
                                     SupervisorName = S.FullName,
                                     ApprovedDate = DL.ApprovedDate,

                                     DurationOfCreditHold = DL.DurationOfCreditHold != null ? DL.DurationOfCreditHold : DL.CreditOffHold != null ?
           DL.DurationOfCreditHold :
           Convert.ToString(-(Convert.ToDateTime(DL.CreditOnHold).Subtract(DateTime.Now).Days)),

                                     ReasonforCredithold = DL.ReasonforCredithold,
                                     ShiftId = DL.ShiftId,
                                     ShiftName = DL.ShiftId == "1" ? "Regular" : "Night",
                                     CreditOnHold = DL.CreditOnHold,
                                     CreditOffHold = DL.CreditOffHold,
                                     DisplayCreditHoldOn = DL.CreditOnHold != null ? Convert.ToDateTime(DL.CreditOnHold).ToString("MM/dd/yyyy") : "",
                                     DisplayCreditHoldOff = DL.CreditOffHold != null ? Convert.ToDateTime(DL.CreditOffHold).ToString("MM/dd/yyyy") : "",
                                     ActionPlanToPreventCredithold = DL.ActionPlanToPreventCredithold,
                                     ReasonforEscalation = DL.ReasonforEscalation,
                                     ActionPlanToPreventEscalation = DL.ActionPlanToPreventEscalation,
                                     DisplayStatementRequestedDate = DL.StatementRequestedDate != null ? Convert.ToDateTime(DL.StatementRequestedDate).ToString("MM/dd/yy") : "",
                                     DisplayStatementReceivedDate = DL.StatementReceivedDate != null ? Convert.ToDateTime(DL.StatementReceivedDate).ToString("MM/dd/yy") : "",
                                     DisplayStatementMatchCompleteDate = DL.StatementMatchCompleteDate != null ? Convert.ToDateTime(DL.StatementMatchCompleteDate).ToString("MM/dd/yy") : "",
                                     DisplaySettlementDate = DL.SettlementDate != null ? Convert.ToDateTime(DL.SettlementDate).ToString("MM/dd/yy") : "",
                                     IsApproved = DL.IsApproved,
                                     DisplayApprovedDate = DL.ApprovedDate != null ? Convert.ToDateTime(DL.ApprovedDate).ToString("MM/dd/yy") : "",
                                     ApprovedBy = DL.ApprovedBy,
                                     CreatedDate = DL.CreatedDate,
                                     DisplayCreatedDate = Convert.ToDateTime(DL.CreatedDate).ToString("MM/dd/yy"),
                                     CurrentStatus = DL.CurrentStatus

                                 }).ToList();

            return deltaLogslist;
        }

        public List<DeltaLoged> GetHistoryDeltaLogByUser(string LogType, string APRep, bool IsAdmin)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            List<DeltaLoged> deltaLogs = new List<DeltaLoged>();

            if (IsAdmin)
                deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == true && o.LogedDataFor == LogType).ToList();
            else
                deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == true && o.LogedDataFor == LogType && o.APRepID == APRep).ToList();

            var Creditholdlist = (from DL in deltaLogs
                                  join
                                  U in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.APRepID) equals U.Id
                                  join
                                  V in unitOfWork.Vendor.GetAll()
                                  on DL.VendorId equals V.Id
                                  join
                                  S in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.SupervisorId) equals S.Id
                                  join
                                  UP in unitOfWork.POD.GetAll()
                                  on DL.PODId equals Convert.ToString(UP.Id)
                                  join
                                  Approver in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.ApprovedBy) equals Approver.Id

                                  select new DeltaLoged
                                  {
                                      Id = DL.Id,
                                      LogedDataFor = DL.LogedDataFor,
                                      PODId = DL.PODId,
                                      PODName = UP.Name,
                                      APRepID = DL.APRepID,
                                      APRepName = U.FullName,
                                      VendorId = V.Id,
                                      VendorName = V.VendorName,
                                      VendorNumber = V.VendorNumber,
                                      SupervisorId = DL.SupervisorId,
                                      SupervisorName = S.FullName,
                                      ApprovedDate = DL.ApprovedDate,

                                      DurationOfCreditHold = DL.DurationOfCreditHold != null ? DL.DurationOfCreditHold : DL.CreditOffHold != null ?
            DL.DurationOfCreditHold :
            Convert.ToString(-(Convert.ToDateTime(DL.CreditOnHold).Subtract(DateTime.Now).Days)),

                                      ReasonforCredithold = DL.ReasonforCredithold,
                                      ShiftId = DL.ShiftId,
                                      ShiftName = DL.ShiftId == "1" ? "Regular" : "Night",
                                      CreditOnHold = DL.CreditOnHold,
                                      CreditOffHold = DL.CreditOffHold,
                                      DisplayCreditHoldOn = DL.CreditOnHold != null ? Convert.ToDateTime(DL.CreditOnHold).ToString("MM/dd/yy") : "",
                                      DisplayCreditHoldOff = DL.CreditOffHold != null ? Convert.ToDateTime(DL.CreditOffHold).ToString("MM/dd/yy") : "",
                                      ActionPlanToPreventCredithold = DL.ActionPlanToPreventCredithold,
                                      ReasonforEscalation = DL.ReasonforEscalation,
                                      ActionPlanToPreventEscalation = DL.ActionPlanToPreventEscalation,
                                      DisplayStatementRequestedDate = DL.StatementRequestedDate != null ? Convert.ToDateTime(DL.StatementRequestedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementReceivedDate = DL.StatementReceivedDate != null ? Convert.ToDateTime(DL.StatementReceivedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementMatchCompleteDate = DL.StatementMatchCompleteDate != null ? Convert.ToDateTime(DL.StatementMatchCompleteDate).ToString("MM/dd/yy") : "",
                                      DisplaySettlementDate = DL.SettlementDate != null ? Convert.ToDateTime(DL.SettlementDate).ToString("MM/dd/yy") : "",
                                      IsApproved = DL.IsApproved,
                                      DisplayApprovedDate = DL.ApprovedDate != null ? Convert.ToDateTime(DL.ApprovedDate).ToString("MM/dd/yy") : "",
                                      ApprovedBy = DL.ApprovedBy,
                                      AproverName = Approver.FullName,
                                      CreatedDate = DL.CreatedDate,
                                      DisplayCreatedDate = Convert.ToDateTime(DL.CreatedDate).ToString("MM/dd/yy")

                                  }).ToList();

            return Creditholdlist;
        }


        public List<DeltaLoged> GetExportDataDeltaLogByUser(string LogType, string Status, int ApRep, bool IsAdmin)
        {
            //var unitOfWork = new UnitOfWork.UnitOfWork(context);
            List<DeltaLoged> deltaLogs = new List<DeltaLoged>();

            if (LogType == "" && Status == "")
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == ApRep.ToString()).ToList();
            }
            else if (LogType == "" && Status != "")
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false)).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.APRepID == ApRep.ToString()).ToList();
            }
            else if (LogType != "" && Status == "")
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.LogedDataFor == LogType && o.APRepID == ApRep.ToString()).ToList();
            }
            else
            {
                if (IsAdmin)
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == LogType).ToList();
                else
                    deltaLogs = unitOfWork.DeltaLoged.GetAll().Where(o => o.IsApproved == (Status == "1" ? true : false) && o.LogedDataFor == LogType && o.APRepID == ApRep.ToString()).ToList();
            }

            var Creditholdlist = (from DL in deltaLogs
                                  join
                                  U in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.APRepID) equals U.Id
                                  join
                                  V in unitOfWork.Vendor.GetAll()
                                  on DL.VendorId equals V.Id
                                  join
                                  S in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(DL.SupervisorId) equals S.Id
                                  join
                                  UP in unitOfWork.POD.GetAll()
                                  on DL.PODId equals Convert.ToString(UP.Id)

                                  select new DeltaLoged
                                  {
                                      Id = DL.Id,
                                      LogedDataFor = DL.LogedDataFor,
                                      PODId = DL.PODId,
                                      PODName = UP.Name,
                                      APRepID = DL.APRepID,
                                      APRepName = U.FullName,
                                      VendorId = V.Id,
                                      VendorName = V.VendorName,
                                      VendorNumber = V.VendorNumber,
                                      SupervisorId = DL.SupervisorId,
                                      SupervisorName = S.FullName,
                                      ApprovedDate = DL.ApprovedDate,

                                      DurationOfCreditHold = DL.DurationOfCreditHold != null ? DL.DurationOfCreditHold : DL.CreditOffHold != null ?
            DL.DurationOfCreditHold :
            Convert.ToString(-(Convert.ToDateTime(DL.CreditOnHold).Subtract(DateTime.Now).Days)),

                                      ReasonforCredithold = DL.ReasonforCredithold,
                                      ShiftId = DL.ShiftId,
                                      ShiftName = DL.ShiftId == "1" ? "Regular" : "Night",
                                      CreditOnHold = DL.CreditOnHold,
                                      CreditOffHold = DL.CreditOffHold,
                                      DisplayCreditHoldOn = DL.CreditOnHold != null ? Convert.ToDateTime(DL.CreditOnHold).ToString("MM/dd/yy") : "",
                                      DisplayCreditHoldOff = DL.CreditOffHold != null ? Convert.ToDateTime(DL.CreditOffHold).ToString("MM/dd/yy") : "",
                                      ActionPlanToPreventCredithold = DL.ActionPlanToPreventCredithold,
                                      ReasonforEscalation = DL.ReasonforEscalation,
                                      ActionPlanToPreventEscalation = DL.ActionPlanToPreventEscalation,
                                      DisplayStatementRequestedDate = DL.StatementRequestedDate != null ? Convert.ToDateTime(DL.StatementRequestedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementReceivedDate = DL.StatementReceivedDate != null ? Convert.ToDateTime(DL.StatementReceivedDate).ToString("MM/dd/yy") : "",
                                      DisplayStatementMatchCompleteDate = DL.StatementMatchCompleteDate != null ? Convert.ToDateTime(DL.StatementMatchCompleteDate).ToString("MM/dd/yy") : "",
                                      DisplaySettlementDate = DL.SettlementDate != null ? Convert.ToDateTime(DL.SettlementDate).ToString("MM/dd/yy") : "",
                                      IsApproved = DL.IsApproved,
                                      DisplayApprovedDate = DL.ApprovedDate != null ? Convert.ToDateTime(DL.ApprovedDate).ToString("MM/dd/yy") : "",
                                      ApprovedBy = DL.ApprovedBy,
                                      StatusString =DL.IsApproved == true?"Yes":"No",
                                      CreatedDate = DL.CreatedDate,
                                      DisplayCreatedDate = Convert.ToDateTime(DL.CreatedDate).ToString("MM/dd/yy")

                                  }).ToList();

            return Creditholdlist;
        }
    }
}
