﻿using DeltaDashboardCommon.Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DeltaDashboardDAL.Manager
{
    public class UserVendorAssociationDataManager
    {
        private DeltaContext context = new DeltaContext();

        public void CreateUVAssociation(UserVendorAssociation uvAssociation)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            unitOfWork.UserVendorAssociation.Add(uvAssociation);
            unitOfWork.Complete();
        }

        public UserVendorAssociation UpdateUVAssociation(UserVendorAssociation uvAssociation)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            uvAssociation = unitOfWork.UserVendorAssociation.Update(uvAssociation);
            unitOfWork.Complete();
            return uvAssociation;
        }

        public List<Vendor> GetSelectedVendor(int userId)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);

            var vendorlist = (from UVA in unitOfWork.UserVendorAssociation.GetAll().Where(o => o.UserId == userId)
                                  join
                                  V in unitOfWork.Vendor.GetAll() on UVA.VendorId equals V.Id
                                  select new Vendor
                                  {
                                      Id = V.Id,
                                      VendorName = V.VendorName,
                                      VendorNumber = V.VendorNumber
                                  }).OrderBy(o => o.VendorName).ToList();
            return vendorlist;
        }

        public List<UserVendorAssociation> GetAssociationVendor(int userId)        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var vendorlist = unitOfWork.UserVendorAssociation.GetAll().Where(o => o.UserId == userId).ToList();                              
            return vendorlist.ToList();
        }

        public void DeleteUVAssociatio(List<UserVendorAssociation> uvAssociation) {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            foreach (var association in uvAssociation)
            {
                context.Entry(association).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
