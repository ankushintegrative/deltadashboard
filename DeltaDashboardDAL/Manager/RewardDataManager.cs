﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeltaDashboardDAL.Repositories;
using DeltaDashboardCommon.Domain;
using DeltaDashboardCommon.HelperViewModel;
using System.Web;

namespace DeltaDashboardDAL.Manager
{
    public class RewardDataManager
    {
        private DeltaContext context = new DeltaContext();

        UnitOfWork.UnitOfWork unitOfWork = null;

        public RewardDataManager()
        {
            unitOfWork = new UnitOfWork.UnitOfWork(context);
        }

        public int CreateReward(Reward reward)
        {
            unitOfWork.Reward.Add(reward);
            unitOfWork.Complete();
            return reward.Id;
        }

        public Reward UpdateStatus(Reward reward)
        {
            reward = unitOfWork.Reward.Update(reward);
            unitOfWork.Complete();
            return reward;
        }

        public List<Reward> GetRewards()
        {
            unitOfWork = new UnitOfWork.UnitOfWork(context);
            var rewards = (from rw in unitOfWork.Reward.GetAll()
                           select new Reward
                           {
                               Id = rw.Id,
                               Category = rw.Category,
                               RewardTo = rw.RewardTo,
                               Comments = rw.Comments,
                               RewardDate = rw.RewardDate,
                               RewardDateDisplay = rw.RewardDate != null ? Convert.ToDateTime(rw.RewardDate).ToString("dd/MMM/yyyy") : ""
                           }).ToList();

            return rewards;
        }
    }
}
