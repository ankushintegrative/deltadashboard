﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Manager
{
    public class ClientFeedbackDataManager
    {
        private DeltaContext context = new DeltaContext();

        public void CreateFeedback(ClientFeedback clinetFeedback)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            unitOfWork.ClientFeedback.Add(clinetFeedback);
            unitOfWork.Complete();
        }

        public ClientFeedback UpdateFeedback(ClientFeedback clinetFeedback)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            clinetFeedback = unitOfWork.ClientFeedback.Update(clinetFeedback);
            unitOfWork.Complete();
            return clinetFeedback;
        }

        public List<ClientFeedback> GetClientFeedback(string month, string year)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var ClientFeedbackList = (from CF in unitOfWork.ClientFeedback.GetAll().Where(o => o.Month == month && o.Year == year)
                                  join
                                  U in unitOfWork.User.GetAll()
                                  on Convert.ToInt32(CF.ApRepId) equals U.Id                                   
                                  select new ClientFeedback
                                  {
                                      Id = CF.Id,
                                      ApRepId = CF.ApRepId,
                                      ApRepName = U.FullName,
                                      Month = CF.Month,
                                      Year = CF.Year,
                                      CSRType = CF.CSRType,
                                      FeedbackOption = CF.FeedbackOption,
                                      CreatedBy = CF.CreatedBy,
                                      CreatedDate = CF.CreatedDate,
                                      UserId = U.Id,
                                      RoleId = U.Role,
                                      Comments = CF.Comments // Added by Shraddha on 14-Jun-2022
                                  }).ToList();

            return ClientFeedbackList;
        }

        public List<ClientFeedback> GetAllClientFeedback()
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var ClientFeedbackList = (from CF in unitOfWork.ClientFeedback.GetAll()
                                      join
                                      U in unitOfWork.User.GetAll()
                                      on Convert.ToInt32(CF.ApRepId) equals U.Id
                                      select new ClientFeedback
                                      {
                                          Id = CF.Id,
                                          ApRepId = CF.ApRepId,
                                          ApRepName = U.FullName,
                                          Month = CF.Month,
                                          Year = CF.Year,
                                          CSRType = CF.CSRType,
                                          FeedbackOption = CF.FeedbackOption,
                                          CreatedBy = CF.CreatedBy,
                                          CreatedDate = CF.CreatedDate,
                                          UserId = U.Id,
                                          RoleId = U.Role
                                      }).ToList();

            return ClientFeedbackList;
        }

        public List<User> GetPendingAPRep(string month, string year)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);

            var ClientFeedbackPending = (from U in unitOfWork.User.GetAll()
                           where !unitOfWork.ClientFeedback.GetAll().Where(o => o.Month == month && o.Year == year).Select(o => o.ApRepId).Distinct().Any(c => Convert.ToInt32(c.Value) == U.Id)
                           join UN in unitOfWork.User.GetAll()
                           on Convert.ToInt32(U.SupervisorId) equals UN.Id
                           select new User { APRepName = U.FullName,
                               SupervisorName = UN.FullName,
                               Id = U.Id,
                               Role = U.Role
                           }).ToList();

            return ClientFeedbackPending.Where(o => o.Role == "1").ToList();
        }

        public List<User> GetPendingAPRepDataProcessing(string month, string year)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);

            var ClientFeedbackPending = (from U in unitOfWork.User.GetAll()
                                         where !unitOfWork.ClientFeedback.GetAll().Where(o => o.Month == month && o.Year == year).Select(o => o.ApRepId).Distinct().Any(c => Convert.ToInt32(c.Value) == U.Id)
                                         join UN in unitOfWork.User.GetAll()
                                         on Convert.ToInt32(U.SupervisorId) equals UN.Id
                                         select new User
                                         {
                                             APRepName = U.FullName,
                                             SupervisorName = UN.FullName,
                                             Id = U.Id,
                                             Role = U.Role
                                         }).ToList();

            return ClientFeedbackPending.Where(o => o.Role == "6").ToList();
        }

        //public List<User> GetCompleteListOfAPRep()
        //{
        //    var unitOfWork = new UnitOfWork.UnitOfWork(context);
        //    var ClientFeedbackCompleteListOfAPRep = (from CF in unitOfWork.ClientFeedback.GetAll().Select(o => o.ApRepId).Distinct()
        //                                 join
        //                                 U in unitOfWork.User.GetAll()
        //                                 on Convert.ToInt32(CF.Value) equals U.Id
        //                                 join
        //                                 UN in unitOfWork.User.GetAll()
        //                                 on Convert.ToInt32(U.SupervisorId) equals UN.Id
        //                                 select new User
        //                                 {
        //                                     Id = U.Id
        //                                 }).ToList();

        //    return ClientFeedbackCompleteListOfAPRep;
        //}

        public List<User> GetCompleteAPRep(string month, string year)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var ClientFeedbackComplete = (from CF in unitOfWork.ClientFeedback.GetAll().Where(o => o.Month == month && o.Year == year).Select(o => o.ApRepId).Distinct()
                                          join
                                         U in unitOfWork.User.GetAll()
                                         on Convert.ToInt32(CF.Value) equals U.Id
                                         join
                                         UN in unitOfWork.User.GetAll()
                                         on Convert.ToInt32(U.SupervisorId) equals UN.Id
                                         select new User
                                         {
                                             APRepName = U.FullName,
                                             SupervisorName = UN.FullName,
                                             Id = U.Id,
                                             Role = U.Role
                                         }).ToList();

            return ClientFeedbackComplete;
        }

        public List<ClientFeedback> GetDataByApRep(int aprepId, string month, string year)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.ClientFeedback.GetDataByApRep(aprepId, month, year);
        }
    }
}
