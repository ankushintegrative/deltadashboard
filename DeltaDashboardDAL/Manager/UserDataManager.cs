﻿using DeltaDashboardCommon.Domain;
using System.Collections.Generic;
using System.Linq;

namespace DeltaDashboardDAL.Manager
{
    public class UserDataManager
    {
        private DeltaContext context = new DeltaContext();

        public void CreateUser(User user)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            unitOfWork.User.Add(user);
            unitOfWork.Complete();
        }

        public User ValidateLogin(string UserName, string Password)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            User userLogin = new User();
            userLogin = unitOfWork.User.GetAll().Where(o => o.UserName == UserName && o.Password == Password).FirstOrDefault();
            if (userLogin != null)
            {
                userLogin.IsValidUser = true;
            }
            else
            {
                userLogin = new User();
                userLogin.IsValidUser = false;
                userLogin.ErrorMsg = "Please provide correct User Id and Password.";
            }

            return userLogin;
        }

        public User GetUserByUserName(string userName)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            User user = unitOfWork.User.GetUserByUserName(userName);
            if (user == null) 
                return null;
            
            return user;
        }

        public User UpdateUser(User user)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            user = unitOfWork.User.Update(user);
            unitOfWork.Complete();
            return user;
        }

        public List<User> GetUserList()
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            // Where condition added by Shraddha on 29-Jun-2022
            return unitOfWork.User.GetAll().Where(u => u.isDeltaTeam == true).OrderBy(o => o.FullName).ToList();
        }

        public List<Vendor> GetVendorList()
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.Vendor.GetAll().OrderBy(o => o.VendorName).ToList();
        }


       public List<UserRole> GetUserRolesList()
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.UserRole.GetAll().ToList();
        }

        public List<POD> GetPODList()
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var pod = (from pd in unitOfWork.POD.GetAll()
                           select new POD
                           {
                               Id = pd.Id,
                               Name = pd.Name,
                               FullName = pd.Name
                           }).ToList();

            return pod;
        }
    }
}
