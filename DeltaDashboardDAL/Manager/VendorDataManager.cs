﻿using DeltaDashboardCommon.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeltaDashboardDAL.Manager
{
    public class VendorDataManager
    {
        private DeltaContext context = new DeltaContext();

        public void CreateVendor(Vendor vendor)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            unitOfWork.Vendor.Add(vendor);
            unitOfWork.Complete();
        }

        public Vendor UpdateVendor(Vendor vendor)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            vendor = unitOfWork.Vendor.Update(vendor);
            unitOfWork.Complete();
            return vendor;
        }

        public List<Vendor> GetVendorList()
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.Vendor.GetAll().OrderBy(o => o.VendorName).ToList();
        }

        public List<Vendor> GetVendorsByIndividualAPRep(int APRepUserId)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var VendorList = (from D in unitOfWork.DeltaLoged.GetAll().Where(o => o.APRepID == Convert.ToString(APRepUserId)).Select(o => o.VendorId).Distinct()
                              join V in unitOfWork.Vendor.GetAll()
                              on D.Value equals V.Id

                              select new Vendor
                              {
                                  Id = V.Id,
                                  VendorName = V.VendorName,
                                  VendorNumber = V.VendorNumber
                              }).OrderBy(o => o.VendorName).ToList();
            
            return VendorList;
        }

        public List<Vendor> GetVendorsByAPRep(int APRepUserId)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            var VendorList = (from UVA in unitOfWork.UserVendorAssociation.GetAll().Where(o => o.UserId == APRepUserId)
                              join
                             V in unitOfWork.Vendor.GetAll()
                              on UVA.VendorId equals V.Id
                              select new Vendor
                              {
                                  Id = V.Id,
                                  VendorName = V.VendorName,
                                  VendorNumber = V.VendorNumber
                              }).OrderBy(o => o.VendorName).ToList();

            return VendorList;
        }

        public Vendor GetVendorByNumber(string number)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            Vendor vendor = unitOfWork.Vendor.GetVendorByNumber(number);
            if (vendor == null)
                return null;

            return vendor;
        }

        public Vendor GetVendorById(int id)
        {
            var unitOfWork = new UnitOfWork.UnitOfWork(context);
            return unitOfWork.Vendor.Get(id);
        }
    }
}
